/**
 * 
 */
package ca.cyberseclab.avp.core;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * @author sfrenette
 */
public class LabCloser extends JFrame
{
	private static final long serialVersionUID = 4552841338706836456L;
	private final Thread labThread;

	public LabCloser()
	{
		super("VL4M");

		final LabManagerMaster labMaster = new LabManagerMaster("./scenarios/SampleScenario3.xml");
		this.labThread = new Thread(labMaster);
		this.labThread.start();

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.addWindowListener(this.onClose);

		final JLabel label = new JLabel("Close this window to exit the lab.");
		this.getContentPane().add(label);

		this.pack();
		this.setVisible(true);

		try
		{
			this.labThread.join();
			this.dispose();
		}
		catch (final InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private final WindowAdapter onClose = new WindowAdapter()
	{
		@Override
		public void windowClosing(final WindowEvent e)
		{
			final Thread shutdownHook = new Thread(new ShutdownHook());
			shutdownHook.start();
			try
			{
				shutdownHook.join();
			}
			catch (final InterruptedException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	};

}

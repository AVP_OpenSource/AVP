/**
 * 
 */
package ca.cyberseclab.avp.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.NoSuchElementException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;

import ca.cyberseclab.avp.core.action.exception.ActionCriticalException;
import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;
import ca.cyberseclab.avp.core.device.genymotion.GenymotionDevice;
import ca.cyberseclab.avp.core.device.google.CustomGoogleDevice;
import ca.cyberseclab.avp.core.device.google.GoogleDevice;
import ca.cyberseclab.avp.core.device.google.TaintDroidDevice;
import ca.cyberseclab.avp.core.device.physical.PhysicalDevice;
import ca.cyberseclab.avp.core.exception.LabExecutionException;
import ca.cyberseclab.avp.scenario.AbstractScenarioParser;
import ca.cyberseclab.avp.scenario.ParserException;
import ca.cyberseclab.avp.scenario.Scenario;
import ca.cyberseclab.avp.scenario.Scene;
import ca.cyberseclab.avp.scenario.XMLScenarioParser;
import ca.cyberseclab.avp.util.DateStringUtil;
import ca.cyberseclab.avp.util.command.Command;
import ca.cyberseclab.avp.util.command.CommandLineWrapper;
import ca.cyberseclab.avp.util.logger.LogViewer;
import ca.cyberseclab.avp.util.logger.LoggerUtil;
import ca.cyberseclab.avp.util.setting.Settings;
import ch.qos.logback.classic.Logger;

/**
 * @author sfrenette
 */
public class LabManagerMaster implements Runnable
{
	public static void main(final String[] args)
	{
		if (args.length == 1)
		{
			final LabManagerMaster labMaster = new LabManagerMaster(args[0]);
			labMaster.run();
		}
		else
		{

			final LabManagerMaster labMaster = new LabManagerMaster("./scenarios/SampleScenario1.xml");
			labMaster.run();
		}
	}

	final static Map<String, HashSet<AbstractDevice>> devices = new HashMap<String, HashSet<AbstractDevice>>();
	final static Map<String, AbstractDevice> usedDevices = new HashMap<String, AbstractDevice>();

	private static String scenarioResultFolderPath;

	public static String getScenarioResultFolderPath()
	{
		return LabManagerMaster.scenarioResultFolderPath;
	}

	private Scenario scenario;
	private final String scenarioPath;

	public LabManagerMaster(final String scenarioPath)
	{
		this(scenarioPath, null);
	}

	public LabManagerMaster(final String scenarioPath, final LogViewer logViewer)
	{
		Runtime.getRuntime().addShutdownHook(new Thread(new ShutdownHook()));
		LoggerUtil.setLogViewer(logViewer);
		this.scenarioPath = scenarioPath;

		try
		{
			Settings.load("config/LabSettings.txt");
			this.prepareScenarioResult();
			this.buildDeviceList();
			final AbstractScenarioParser scenarioParser = new XMLScenarioParser(scenarioPath, this);
			this.scenario = scenarioParser.parse();
		}
		catch (final ParserException | IOException e)
		{
			LoggerUtil.getLogger("error").error(e.getMessage());
			new Thread(new ShutdownHook()).start();
		}
	}

	@Override
	public void run()
	{
		LoggerUtil.getLogger("devices");
		LoggerUtil.getRootLogger();

		this.runScenario();

		new Thread(new ShutdownHook()).start();
	}

	private void runScenario()
	{
		for (final Scene scene : this.scenario.getActiveScenes())
		{
			this.runScene(scene);
		}
	}

	private void runScene(final Scene scene)
	{
		while (scene.hasMoreActions())
		{
			try
			{
				scene.executeNextAction();
			}
			catch (final ActionCriticalException e)
			{
				LoggerUtil.getLogger("error").error(e.getClass().getSimpleName() + ": " + e.getMessage());
				System.exit(1);
			}
			catch (final ActionExecutionException e)
			{
				//TODO Clearer error logging (stack trace, etc.)
				LoggerUtil.getLogger("error").error(e.getClass().getSimpleName() + ": " + e.getMessage());
			}
		}
	}

	// DEVICES
	public void assignIDToDevice(final String ID, final String deviceTechnology, final String deviceName)
	{
		for (final AbstractDevice device : LabManagerMaster.devices.get(deviceTechnology))
		{
			if (device.getName().equals(deviceName))
			{
				device.assignID(ID);
				LabManagerMaster.usedDevices.put(ID, device);
				return;
			}
		}
		final String errorMessage = String.format("%s - assignIDToDevice: There's no device with technology %s and name %s.", this.getClass().getSimpleName(), deviceTechnology, deviceName);
		LoggerUtil.getLogger("error").error(errorMessage);
		throw new NoSuchElementException(errorMessage);
	}

	//<lafrancef>
	//TODISCUSS Do we want/need to guarantee that system ids will be unique?
	public void assignSystemIdToDevice(final String systemId, final String deviceTechnology, final String deviceName)
	{
		for (final AbstractDevice device : LabManagerMaster.devices.get(deviceTechnology))
		{
			if (device.getName().equals(deviceName))
			{
				device.assignSystemId(systemId);
				return;
			}
		}
		final String errorMessage = String.format("%s - assignIDToDevice: There's no device with technology %s and name %s.", this.getClass().getSimpleName(), deviceTechnology, deviceName);
		LoggerUtil.getLogger("error").error(errorMessage);
		throw new NoSuchElementException(errorMessage);
	}

	//</lafrancef>

	public AbstractDevice getDeviceFromID(final String ID)
	{
		for (final AbstractDevice device : LabManagerMaster.usedDevices.values())
		{
			if (device.getScenarioID().equals(ID))
			{
				return device;
			}
		}
		final String errorMessage = String.format("%s - getDeviceFromID: There's no device with ID %s.", this.getClass().getSimpleName(), ID);
		LoggerUtil.getLogger("error").error(errorMessage);
		throw new NoSuchElementException(errorMessage);
	}

	private void buildDeviceList() throws FileNotFoundException
	{
		if (Settings.contains(Settings.GOOGLE_AVD_PATH_KEY))
		{
			final HashSet<String> devicesNames = this.getGoogleDevicesNames();
			LabManagerMaster.devices.put(AbstractDevice.TECH_GOOGLE, this.getGoogleDevices(devicesNames));
			LabManagerMaster.devices.put(AbstractDevice.TECH_GOOGLE_MODIFIED, this.getCustomGoogleDevices(devicesNames));
			LabManagerMaster.devices.put(AbstractDevice.TECH_TAINTDROID, this.getTaintDroidDevices(devicesNames));
		}

		//<lafrancef>
		if (Settings.contains(Settings.GENYSHELL_PATH_KEY))
		{
			LabManagerMaster.devices.put(AbstractDevice.TECH_GENYMOTION, this.getGenymotionDevices());
		}

		LabManagerMaster.devices.put(AbstractDevice.TECH_PHYSICAL, this.getPhysicalDevices());
		//</lafrancef>
	}

	private HashSet<AbstractDevice> getGoogleDevices(final HashSet<String> devicesNames)
	{
		final HashSet<AbstractDevice> googleSet = new HashSet<AbstractDevice>();
		for (final String deviceName : devicesNames)
		{
			googleSet.add(new GoogleDevice(deviceName));
		}
		return googleSet;
	}

	private HashSet<AbstractDevice> getCustomGoogleDevices(final HashSet<String> devicesNames)
	{
		final HashSet<AbstractDevice> customGoogleSet = new HashSet<AbstractDevice>();
		for (final String deviceName : devicesNames)
		{
			customGoogleSet.add(new CustomGoogleDevice(deviceName));
		}
		return customGoogleSet;
	}

	private HashSet<AbstractDevice> getTaintDroidDevices(final HashSet<String> devicesNames)
	{
		final HashSet<AbstractDevice> tdroidSet = new HashSet<AbstractDevice>();
		for (final String deviceName : devicesNames)
		{
			tdroidSet.add(new TaintDroidDevice(deviceName));
		}
		return tdroidSet;
	}

	private HashSet<String> getGoogleDevicesNames() throws FileNotFoundException
	{
		final String googleAVDPath = Settings.get(Settings.GOOGLE_AVD_PATH_KEY);
		final File googleAVDFolder = new File(googleAVDPath);
		final String[] directories = googleAVDFolder.list(DirectoryFileFilter.INSTANCE);
		if (directories == null)
		{
			throw new FileNotFoundException("Couldn't find any AVD in the google folder specified in the settings.");
		}

		final HashSet<String> names = new HashSet<String>();
		for (final String device : directories)
		{
			final String avdName = device.substring(0, device.indexOf("."));
			names.add(avdName);
		}

		return names;
	}

	//<lafrancef>
	private HashSet<AbstractDevice> getPhysicalDevices()
	{
		/* Get all connected devices with ADB (normally, a physical device must be plugged in
		 * or network-accessible before the scenario starts) */
		final Logger slaveLogger = LoggerUtil.getLogger("debug");
		final Command getDevices = new Command(Settings.get(Settings.ADB_PATH_KEY))
						.addParameter("devices")
						.setLogger(slaveLogger);

		String cmdResult = "";
		try
		{
			cmdResult = CommandLineWrapper.executeBlockingCommand(getDevices);
		}
		catch (final IOException e)
		{
			final String message = String.format("Failed to execute command %s", getDevices.toString());
			throw new RuntimeException(new LabExecutionException(message, e));
		}

		final HashSet<AbstractDevice> physSet = new HashSet<AbstractDevice>();
		final String[] lines = cmdResult.split("\n");
		for (int i = 0; i < lines.length; i++)
		{
			final int tab = lines[i].indexOf('\t');
			if (tab != -1)
			{
				/* Make sure that we really have a physical device. For now, we use the
				 * ro.product.board property. It's empty for the Google emulator and 
				 * Genymotion. */
				final String deviceName = lines[i].substring(0, tab);
				final Command getProp = new Command(Settings.get(Settings.ADB_PATH_KEY))
								.addParameter("-s", deviceName)
								.addParameter("shell")
								.addParameter("getprop", "ro.product.board");
				try
				{
					cmdResult = CommandLineWrapper.executeBlockingCommand(getProp);
				}
				catch (final IOException e)
				{
					final String message = String.format("Failed to execute command %s", getDevices.toString());
					throw new RuntimeException(new LabExecutionException(message, e));
				}

				if (!cmdResult.isEmpty())
				{
					physSet.add(new PhysicalDevice(deviceName));
				}
			}
		}
		return physSet;
	}

	private HashSet<AbstractDevice> getGenymotionDevices()
	{
		final Command getDevices = new Command(Settings.get(Settings.GENYSHELL_PATH_KEY))
						.addParameter("-c", "devices show"); //-c: execute the command and close

		String result = "";
		try
		{
			result = CommandLineWrapper.executeBlockingCommand(getDevices);
		}
		catch (final IOException e)
		{
			final String message = String.format("Failed to execute command %s", getDevices.toString());
			throw new RuntimeException(new LabExecutionException(message, e));
		}

		final HashSet<AbstractDevice> genySet = new HashSet<AbstractDevice>();
		final String[] lines = result.split("\n");
		int i = 0;
		while (!lines[i].startsWith("Available devices:"))
		{
			i++;
			if (i == lines.length)
			{
				//No devices. Return the empty set now.
				return genySet;
			}
		}

		i += 3; //Skip to beginning of devices
		for (; i < lines.length; i++)
		{
			final String[] fields = lines[i].split("\\|");
			final String nameField = fields[fields.length - 1].trim();
			genySet.add(new GenymotionDevice(nameField));
		}

		return genySet;
	}

	//</lafrancef>

	// SCENARIO RESULTS

	private void prepareScenarioResult() throws IOException
	{
		LabManagerMaster.scenarioResultFolderPath = this.getScenarioResultCompletePath();
		this.copyScenarioToResultFolder();
	}

	private String getScenarioResultCompletePath()
	{
		final String scenarioResultFolderName = this.getScenarioResultFolderName();
		final String scenarioResultParentFolderPath = this.getScenarioResultParentFolderPath();
		return scenarioResultParentFolderPath + "\\" + scenarioResultFolderName;
	}

	private String getScenarioResultParentFolderPath()
	{
		final String result;
		if (Settings.contains(Settings.SCENARIO_RESULTS_PATH_KEY))
		{
			result = Settings.get(Settings.SCENARIO_RESULTS_PATH_KEY);
		}
		else
		{
			result = System.getProperty("user.home") + "\\vl4m\\ScenarioResults";
			final String errorMessage = String.format("Couldn't find the scenario results path in the config file. " +
							"Output is now redirected to the default folder. (%s)", result);
			LoggerUtil.getLogger("warn").warn(errorMessage);
		}
		return result;
	}

	private String getScenarioResultFolderName()
	{
		final StringBuilder pathBuilder = new StringBuilder()
						.append(FilenameUtils.getBaseName(this.scenarioPath))
						.append("_")
						.append(DateStringUtil.get());

		return pathBuilder.toString();
	}

	private void copyScenarioToResultFolder() throws IOException
	{
		final File scenarioFile = new File(this.scenarioPath);
		final File scenarioResultDirectoryFile = new File(LabManagerMaster.scenarioResultFolderPath);

		FileUtils.copyFileToDirectory(scenarioFile, scenarioResultDirectoryFile);
	}

}

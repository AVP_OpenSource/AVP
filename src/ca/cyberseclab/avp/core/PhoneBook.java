/**
 * 
 */
package ca.cyberseclab.avp.core;

import java.util.HashMap;
import java.util.Map;

import ca.cyberseclab.avp.core.device.AbstractDevice;

/**
 * @author sfrenette
 */
public class PhoneBook
{
	private static PhoneBook instance;

	public static PhoneBook getInstance()
	{
		if (PhoneBook.instance == null)
		{
			PhoneBook.instance = new PhoneBook();
		}
		return PhoneBook.instance;
	}

	// Map<PhoneNumber(port), device>
	private final Map<String, AbstractDevice> devices = new HashMap<String, AbstractDevice>();

	public AbstractDevice getDeviceFromNumber(final String number)
	{
		return this.devices.get(number);
	}

	public AbstractDevice getDeviceFromSystemID(final String systemID)
	{
		// TODO currently only working with google devices
		final String port = systemID.substring(systemID.lastIndexOf("-"));
		return this.devices.get(port);
	}

	public boolean containsNumber(final String number)
	{
		return this.devices.containsKey(number);
	}

	public boolean containsDevice(final AbstractDevice device)
	{
		return this.devices.containsValue(device);
	}

	public void put(final String number, final AbstractDevice device)
	{
		this.devices.put(number, device);
	}

	private PhoneBook()
	{

	}
}

/**
 * 
 */
package ca.cyberseclab.avp.core;

import java.io.File;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.action.labAction.StopAVDAction;
import ca.cyberseclab.avp.core.device.AbstractDevice;
import ca.cyberseclab.avp.core.serviceLogger.FileServiceLogger;
import ca.cyberseclab.avp.core.serviceLogger.PortServiceLogger;
import ca.cyberseclab.avp.util.logger.LoggerUtil;

/**
 * @author sfrenette
 */
public class ShutdownHook implements Runnable
{
	@Override
	public void run()
	{
		this.shutdownDevices();
		this.shutdownServiceLoggers();
		//this.shutdownADB();
		this.cleanUpFolders();
	}

	//TODO who should call this function or should it be removed ?
	/*private void shutdownADB()
	{
		// Only kill the adb's we started
		final Command command = new Command("taskkill")
						.addParameter("/IM", "adb.exe", "/F");
		try
		{
			CommandLineWrapper.executeNonblockingCommand(command);
		}
		catch (final IOException e)
		{
			LoggerUtil.getLogger("error").error("{} - Couldn't shutdown ADB...", this.getClass().getSimpleName());
		}

	}*/

	private void cleanUpFolders()
	{
		// cleaning results folder
		final File resultsFolder = new File(FileServiceLogger.RESULTS_FOLDER_PATH);
		final File[] resultFiles = resultsFolder.listFiles();
		if (resultFiles != null)
		{
			for (final File currentFile : resultFiles)
			{
				currentFile.delete();
			}
		}

		// cleaning requests folder
		final File requestsFolder = new File(FileServiceLogger.REQUESTS_FOLDER_PATH);
		final File[] requestsFiles = requestsFolder.listFiles();
		if (requestsFiles != null)
		{
			for (final File currentFile : requestsFiles)
			{
				currentFile.delete();
			}
		}
	}

	private void shutdownDevices()
	{
		for (final AbstractDevice device : LabManagerMaster.usedDevices.values())
		{
			final List<AbstractDevice> deviceList = new ArrayList<AbstractDevice>();
			deviceList.add(device);

			final StopAVDAction stopAction = new StopAVDAction(deviceList);
			try
			{
				stopAction.execute();
			}
			catch (final ActionExecutionException e)
			{
				LoggerUtil.getLogger("error").error("{} - Couldn't shutdown device {}...", this.getClass().getSimpleName(), device.getScenarioID());
			}
		}
	}

	private void shutdownServiceLoggers()
	{
		if (PortServiceLogger.isInstanciated())
		{
			try
			{
				PortServiceLogger.getInstance().interrupt();
			}
			catch (final SocketException e)
			{
				LoggerUtil.getLogger("error").error("{} - Couldn't shutdown {}...", this.getClass().getSimpleName(), PortServiceLogger.class.getSimpleName());
			}
		}

		if (FileServiceLogger.isInstanciated())
		{
			FileServiceLogger.getInstance().interrupt();
		}
	}

}

/**
 * 
 */
package ca.cyberseclab.avp.core.action;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;

/**
 * @author sfrenette
 */
public abstract class AbstractAction
{
	protected List<AbstractDevice> devicesAffected;

	public AbstractAction(final List<AbstractDevice> devicesAffected)
	{
		this.devicesAffected = devicesAffected;
	}

	public void execute() throws ActionExecutionException
	{
		//TODO_Frank catch exceptions here and throw the bundle of excetion after trying on all devices...
		for (final AbstractDevice device : this.devicesAffected)
		{
			device.getLogger().info("Executing " + this.toString());
			this.executeOnDevice(device);
		}
	}

	protected abstract void executeOnDevice(AbstractDevice device) throws ActionExecutionException;

	@Override
	public String toString()
	{
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(String.format("%s on ", this.getClass().getSimpleName()));
		stringBuilder.append(this.toStringDevices());

		return stringBuilder.toString();
	}

	protected String toStringDevices()
	{
		final StringBuilder stringBuilder = new StringBuilder();
		if (this.devicesAffected.isEmpty())
		{
			stringBuilder.append("no devices");
		}
		else if (this.devicesAffected.size() == 1)
		{
			stringBuilder.append("device ");
		}
		else
		{
			stringBuilder.append("devices ");
		}

		for (final AbstractDevice device : this.devicesAffected)
		{
			stringBuilder.append(device.getScenarioID() + " ");
		}

		return stringBuilder.toString();
	}
}

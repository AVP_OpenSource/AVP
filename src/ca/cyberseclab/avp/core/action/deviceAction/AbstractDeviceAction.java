/**
 * 
 */
package ca.cyberseclab.avp.core.action.deviceAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.AbstractAction;
import ca.cyberseclab.avp.core.device.AbstractDevice;


/**
 * @author sfrenette
 */
public abstract class AbstractDeviceAction extends AbstractAction
{
	public AbstractDeviceAction(final List<AbstractDevice> devicesAffected)
	{
		super(devicesAffected);
	}
}

/**
 * 
 */
package ca.cyberseclab.avp.core.action.deviceAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;


/**
 * @author sfrenette
 */
public class CallNumberAction extends AbstractDeviceAction
{

	private final String toNumber;

	/**
	 * @param devicesAffected
	 * @param toNumber
	 */
	public CallNumberAction(List<AbstractDevice> devicesAffected, String toNumber)
	{
		super(devicesAffected);
		this.toNumber = toNumber;
	}

	@Override
	protected void executeOnDevice(AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);
	}

	public String getToNumber()
	{
		return this.toNumber;
	}

}

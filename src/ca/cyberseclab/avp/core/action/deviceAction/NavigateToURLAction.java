/**
 * 
 */
package ca.cyberseclab.avp.core.action.deviceAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;


/**
 * @author sfrenette
 */
public class NavigateToURLAction extends AbstractDeviceAction
{
	private final String url;

	public NavigateToURLAction(final List<AbstractDevice> devicesAffected, final String url)
	{
		super(devicesAffected);
		this.url = url;
	}

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);

	}

	public String getURL()
	{
		return this.url;
	}

}

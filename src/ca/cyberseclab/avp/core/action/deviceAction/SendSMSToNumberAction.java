/**
 * 
 */
package ca.cyberseclab.avp.core.action.deviceAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;


/**
 * @author sfrenette
 */
public class SendSMSToNumberAction extends AbstractDeviceAction
{
	private final String message;
	private final String toNumber;

	public SendSMSToNumberAction(final List<AbstractDevice> devicesAffected, final String message, final String toNumber)
	{
		super(devicesAffected);
		this.message = message;
		this.toNumber = toNumber;
	}

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{// on va aller chercher le numero ici pour les to-device
		device.execute(this);
	}

	public String getMessage()
	{
		return this.message;
	}

	public String getToNumber()
	{
		return this.toNumber;
	}

}

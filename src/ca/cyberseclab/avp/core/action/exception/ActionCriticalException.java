package ca.cyberseclab.avp.core.action.exception;

/**
 * @author sfrenette
 */
public class ActionCriticalException extends ActionExecutionException
{
	private static final long serialVersionUID = -2834825694212485716L;

	public ActionCriticalException()
	{
	}

	public ActionCriticalException(String message)
	{
		super(message);
	}

	public ActionCriticalException(String message, Exception e)
	{
		super(message, e);
	}

}

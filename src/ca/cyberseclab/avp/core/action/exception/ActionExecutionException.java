package ca.cyberseclab.avp.core.action.exception;

/**
 * @author sfrenette
 */
public class ActionExecutionException extends Exception
{
	private static final long serialVersionUID = 8224451127581272702L;

	public ActionExecutionException()
	{
	}

	public ActionExecutionException(final String message)
	{
		super(message);
	}

	public ActionExecutionException(final String message, final Throwable e)
	{
		super(message, e);
	}

	public ActionExecutionException(final Throwable e)
	{
		super(e);
	}
}

package ca.cyberseclab.avp.core.action.exception;

/**
 * @author sfrenette
 */
public class ActionTimeoutException extends ActionExecutionException
{
	private static final long serialVersionUID = -3089141474379893164L;

	public ActionTimeoutException()
	{
	}

	public ActionTimeoutException(String message)
	{
		super(message);
	}

	public ActionTimeoutException(String message, Exception e)
	{
		super(message, e);
	}

}

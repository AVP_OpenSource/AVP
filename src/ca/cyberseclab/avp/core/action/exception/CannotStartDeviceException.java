package ca.cyberseclab.avp.core.action.exception;

/**
 * @author sfrenette
 */
public class CannotStartDeviceException extends ActionCriticalException
{
	private static final long serialVersionUID = -4423559548578240470L;

	public CannotStartDeviceException()
	{
	}

	public CannotStartDeviceException(final String message)
	{
		super(message);
	}

	public CannotStartDeviceException(final String message, final Exception e)
	{
		super(message, e);
	}

}

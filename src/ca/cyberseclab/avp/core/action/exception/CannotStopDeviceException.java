package ca.cyberseclab.avp.core.action.exception;

/**
 * @author lafrancef
 */
public class CannotStopDeviceException extends ActionCriticalException
{
	private static final long serialVersionUID = 5120129490891423463L;

	public CannotStopDeviceException()
	{
	}

	public CannotStopDeviceException(final String message)
	{
		super(message);
	}

	public CannotStopDeviceException(final String message, final Exception e)
	{
		super(message, e);
	}

}

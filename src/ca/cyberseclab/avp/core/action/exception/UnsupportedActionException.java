/**
 * 
 */
package ca.cyberseclab.avp.core.action.exception;

/**
 * @author lafrancef
 */
public class UnsupportedActionException extends ActionExecutionException
{
	private static final long serialVersionUID = -3210163260043663057L;

	public UnsupportedActionException()
	{
	}

	public UnsupportedActionException(final String message)
	{
		super(message);
	}

	public UnsupportedActionException(final String message, final Exception e)
	{
		super(message, e);
	}
}

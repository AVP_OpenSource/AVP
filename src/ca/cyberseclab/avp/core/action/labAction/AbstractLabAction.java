/**
 * 
 */
package ca.cyberseclab.avp.core.action.labAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.AbstractAction;
import ca.cyberseclab.avp.core.device.AbstractDevice;


/**
 * @author sfrenette
 */
public abstract class AbstractLabAction extends AbstractAction
{
	public AbstractLabAction(final List<AbstractDevice> devicesAffected)
	{
		super(devicesAffected);
	}
}

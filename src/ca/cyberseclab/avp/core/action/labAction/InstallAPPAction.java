/**
 * 
 */
package ca.cyberseclab.avp.core.action.labAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;

/**
 * @author sfrenette
 */
public class InstallAPPAction extends AbstractLabAction
{
	private final String localPathToAPK;
	private final boolean reinstall;

	public InstallAPPAction(final List<AbstractDevice> devicesAffected, final String localPathToAPK, final boolean reinstall)
	{
		super(devicesAffected);
		this.localPathToAPK = localPathToAPK;
		this.reinstall = reinstall;
	}

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);
	}

	public String getlocalPathToAPK()
	{
		return this.localPathToAPK;
	}

	public boolean getReinstall()
	{
		return this.reinstall;
	}

	@Override
	public String toString()
	{
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(String.format("InstallAppAction: installing app(%s) on ", this.localPathToAPK));
		stringBuilder.append(this.toStringDevices());

		return stringBuilder.toString();
	}
}

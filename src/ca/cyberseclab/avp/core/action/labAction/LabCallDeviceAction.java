/**
 * 
 */
package ca.cyberseclab.avp.core.action.labAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;

/**
 * @author sfrenette
 */
public class LabCallDeviceAction extends AbstractLabAction
{
	private final String fromNumber;

	public LabCallDeviceAction(final List<AbstractDevice> devicesAffected, final String fromNumber)
	{
		super(devicesAffected);
		this.fromNumber = fromNumber;
	}

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);

	}

	public String getFromNumber()
	{
		return fromNumber;
	}

}

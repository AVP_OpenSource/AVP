/**
 * 
 */
package ca.cyberseclab.avp.core.action.labAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;

/**
 * @author sfrenette
 */
public class LabSendSMSToDeviceAction extends AbstractLabAction
{
	private final String message;
	private final String fromNumber;

	public LabSendSMSToDeviceAction(final List<AbstractDevice> devicesAffected, final String message, final String fromNumber)
	{
		super(devicesAffected);
		this.message = message;
		this.fromNumber = fromNumber;
	}

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);
	}

	public String getMessage()
	{
		return this.message;
	}

	public String getFromNumber()
	{
		return fromNumber;
	}

}

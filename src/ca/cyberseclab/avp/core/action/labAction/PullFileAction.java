/**
 * 
 */
package ca.cyberseclab.avp.core.action.labAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;


/**
 * @author sfrenette
 */
public class PullFileAction extends AbstractLabAction
{
	private final String remoteFilePath;
	private final String localFilePath;

	public PullFileAction(final List<AbstractDevice> devicesAffected, final String remoteFilePath, final String localFilePath)
	{
		super(devicesAffected);
		this.remoteFilePath = remoteFilePath;
		this.localFilePath = localFilePath;
	}

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);
	}

	public String getRemoteFilePath()
	{
		return this.remoteFilePath;
	}

	public String getLocalFilePath()
	{
		return this.localFilePath;
	}

	@Override
	public String toString()
	{
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(String.format("PullFileAction: pulling file(%s) to destination(%s) on ",
						this.remoteFilePath,
						this.localFilePath));
		stringBuilder.append(this.toStringDevices());

		return stringBuilder.toString();
	}
}

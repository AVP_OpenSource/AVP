/**
 * 
 */
package ca.cyberseclab.avp.core.action.labAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;


/**
 * @author sfrenette
 */
public class PushFileAction extends AbstractLabAction
{
	private final String localFilePath;
	private final String remoteFilePath;

	public PushFileAction(final List<AbstractDevice> devicesAffected, final String localFilePath, final String remoteFilePath)
	{
		super(devicesAffected);
		this.localFilePath = localFilePath;
		this.remoteFilePath = remoteFilePath;
	}

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);
	}

	public String getLocalFilePath()
	{
		return this.localFilePath;
	}

	public String getRemoteFilePath()
	{
		return this.remoteFilePath;
	}

	@Override
	public String toString()
	{
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(String.format("PushFileAction: pushing file(%s) to destination(%s) on ",
						this.localFilePath,
						this.remoteFilePath));
		stringBuilder.append(this.toStringDevices());

		return stringBuilder.toString();
	}
}

/**
 * 
 */
package ca.cyberseclab.avp.core.action.labAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;


/**
 * @author sfrenette
 */
public class ShellCommandAction extends AbstractLabAction
{
	private final String command;

	public ShellCommandAction(final List<AbstractDevice> devicesAffected, final String commandStr)
	{
		super(devicesAffected);
		this.command = commandStr;
	}

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);
	}

	public String getCommand()
	{
		return this.command;
	}

	@Override
	public String toString()
	{
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(String.format("ShellCommandAction: executing command(%s) on ",
						this.command.toString()));
		stringBuilder.append(this.toStringDevices());

		return stringBuilder.toString();
	}
}

/**
 * 
 */
package ca.cyberseclab.avp.core.action.labAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;

/**
 * @author sfrenette
 */
public class StartAPPAction extends AbstractLabAction
{
	private final String localPathToAPK;

	public StartAPPAction(final List<AbstractDevice> devicesAffected, final String localPathToAPK)
	{
		super(devicesAffected);
		this.localPathToAPK = localPathToAPK;
	}

	public String getLocalPathToAPK()
	{
		return this.localPathToAPK;
	}

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);
	}
}

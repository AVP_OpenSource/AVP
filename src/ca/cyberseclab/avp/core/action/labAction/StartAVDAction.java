/**
 * 
 */
package ca.cyberseclab.avp.core.action.labAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;

/**
 * @author sfrenette
 */
public class StartAVDAction extends AbstractLabAction
{
	private final boolean persistent;

	public StartAVDAction(final List<AbstractDevice> devicesAffected, final boolean persistent)
	{
		super(devicesAffected);
		this.persistent = persistent;
	}

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);
	}

	public boolean isPersistent()
	{
		return this.persistent;
	}
}

/**
 * 
 */
package ca.cyberseclab.avp.core.action.labAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;

/**
 * @author sfrenette
 */
public class StartMonkeyAction extends AbstractLabAction
{
	private final int numberOfEvents;
	private final String packageName;
	private final String localPathToApk;

	public StartMonkeyAction(final List<AbstractDevice> devicesAffected, final int numberOfEvents, final String packageName, final String apkName)
	{
		super(devicesAffected);
		this.numberOfEvents = numberOfEvents;
		this.packageName = packageName;
		this.localPathToApk = apkName;
	}

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);
	}

	public int getNumberOfEvents()
	{
		return this.numberOfEvents;
	}

	public boolean hasPackageName()
	{
		return (this.packageName != null) && !this.packageName.isEmpty();
	}

	public String getPackageName()
	{
		return this.packageName;
	}

	public String getLocalPathToApk()
	{
		return this.localPathToApk;
	}

}

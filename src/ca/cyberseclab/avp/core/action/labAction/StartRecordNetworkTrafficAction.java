/**
 * 
 */
package ca.cyberseclab.avp.core.action.labAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;

/**
 * @author sfrenette
 */
public class StartRecordNetworkTrafficAction extends AbstractLabAction
{
	private String targetInterface;

	public StartRecordNetworkTrafficAction(final List<AbstractDevice> devicesAffected)
	{
		super(devicesAffected);
		this.targetInterface = "";
	}

	//<lafrancef>
	public StartRecordNetworkTrafficAction(final List<AbstractDevice> devicesAffected, final String interf)
	{
		this(devicesAffected);
		this.targetInterface = interf;
	}

	public String getTargetInterface()
	{
		return this.targetInterface;
	}

	//<//lafrancef>

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);
	}

}

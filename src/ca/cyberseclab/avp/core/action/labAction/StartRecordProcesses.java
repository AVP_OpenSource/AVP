package ca.cyberseclab.avp.core.action.labAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;

/**
 * @author sfrenette
 */
public class StartRecordProcesses extends AbstractLabAction
{
	public StartRecordProcesses(final List<AbstractDevice> devicesAffected)
	{
		super(devicesAffected);
	}

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);
	}
}

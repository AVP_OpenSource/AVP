/**
 * 
 */
package ca.cyberseclab.avp.core.action.labAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;

/**
 * @author lafrancef
 */
public class StartRecordTaintDroid extends AbstractLabAction
{

	/**
	 * @param devicesAffected
	 */
	public StartRecordTaintDroid(final List<AbstractDevice> devicesAffected)
	{
		super(devicesAffected);
	}

	/* (non-Javadoc)
	 * @see ca.thalesgroup.vl4m.core.action.AbstractAction#executeOnDevice(ca.thalesgroup.vl4m.core.device.AbstractDevice)
	 */
	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);
	}

}

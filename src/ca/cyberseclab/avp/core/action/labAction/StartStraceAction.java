/**
 * 
 */
package ca.cyberseclab.avp.core.action.labAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;

/**
 * @author sfrenette
 */
public class StartStraceAction extends AbstractLabAction
{
	private final String processName;

	public StartStraceAction(final List<AbstractDevice> devicesAffected, final String processName)
	{
		super(devicesAffected);
		this.processName = processName;
	}

	public String getProcessName()
	{
		return this.processName;
	}

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);
	}
}

/**
 * 
 */
package ca.cyberseclab.avp.core.action.labAction;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;
import ca.cyberseclab.avp.util.logger.LoggerUtil;

/**
 * @author sfrenette
 */
public class WaitAction extends AbstractLabAction
{
	private final long amountInMs;

	public WaitAction(final long amount)
	{
		super(null);
		this.amountInMs = amount;
	}

	@Override
	public void execute() throws ActionExecutionException
	{
		try
		{
			LoggerUtil.getLogger("debug").trace("Executing " + this.toString());
			Thread.sleep(this.amountInMs);
		}
		catch (final InterruptedException e)
		{
			// An interrupt should never occur here. If it does, simply continue the program.
			final String errorMessage = "WaitAction's sleep has been interrupted.";
			LoggerUtil.getLogger("error").error(errorMessage);
		}
	}

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
	}

	@Override
	public String toString()
	{
		final String result = String.format("WaitAction: waiting for %s ms.", this.amountInMs);
		return result;
	}

}

/**
 * 
 */
package ca.cyberseclab.avp.core.action.labAction;

import java.util.List;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.device.AbstractDevice;


/**
 * @author sfrenette
 */
public class WaitForDeviceBootAction extends AbstractLabAction
{
	private final long checkStatusTimeoutTimeMS;
	private final long checkStatusIntervalTimeMS;

	public WaitForDeviceBootAction(final List<AbstractDevice> devicesAffected, final long checkStatusTimeoutTimeMS, final long checkStatusIntervalTimeMS)
	{
		super(devicesAffected);
		this.checkStatusTimeoutTimeMS = checkStatusTimeoutTimeMS;
		this.checkStatusIntervalTimeMS = checkStatusIntervalTimeMS;
	}

	@Override
	protected void executeOnDevice(final AbstractDevice device) throws ActionExecutionException
	{
		device.execute(this);
	}

	public long checkStatusTimeoutTimeMS()
	{
		return this.checkStatusTimeoutTimeMS;
	}

	public long checkStatusIntervalTimeMS()
	{
		return this.checkStatusIntervalTimeMS;
	}

	@Override
	public String toString()
	{
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(String.format("WaitForDeviceBootAction: waiting for "));
		stringBuilder.append(this.toStringDevices());
		stringBuilder.append("to boot");

		return stringBuilder.toString();
	}
}

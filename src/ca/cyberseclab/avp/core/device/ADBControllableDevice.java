/**
 *
 */
package ca.cyberseclab.avp.core.device;

import java.io.IOException;
import java.net.SocketException;
import java.util.Map.Entry;

import ca.cyberseclab.avp.core.LabManagerMaster;
import ca.cyberseclab.avp.core.action.deviceAction.CallNumberAction;
import ca.cyberseclab.avp.core.action.deviceAction.NavigateToURLAction;
import ca.cyberseclab.avp.core.action.deviceAction.SendSMSToNumberAction;
import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.action.exception.ActionTimeoutException;
import ca.cyberseclab.avp.core.action.exception.UnsupportedActionException;
import ca.cyberseclab.avp.core.action.labAction.InstallAPPAction;
import ca.cyberseclab.avp.core.action.labAction.LabSendSMSToDeviceAction;
import ca.cyberseclab.avp.core.action.labAction.PullFileAction;
import ca.cyberseclab.avp.core.action.labAction.PushFileAction;
import ca.cyberseclab.avp.core.action.labAction.ShellCommandAction;
import ca.cyberseclab.avp.core.action.labAction.StartAPPAction;
import ca.cyberseclab.avp.core.action.labAction.StartMonkeyAction;
import ca.cyberseclab.avp.core.action.labAction.StartRecordCallAction;
import ca.cyberseclab.avp.core.action.labAction.StartRecordNetworkTrafficAction;
import ca.cyberseclab.avp.core.action.labAction.StartRecordProcesses;
import ca.cyberseclab.avp.core.action.labAction.StartRecordSMSAction;
import ca.cyberseclab.avp.core.action.labAction.StartStraceAction;
import ca.cyberseclab.avp.core.action.labAction.StopRecordCallAction;
import ca.cyberseclab.avp.core.action.labAction.StopRecordNetworkTrafficAction;
import ca.cyberseclab.avp.core.action.labAction.StopRecordSMSAction;
import ca.cyberseclab.avp.core.action.labAction.StopStraceAction;
import ca.cyberseclab.avp.core.action.labAction.WaitForAppInstallAction;
import ca.cyberseclab.avp.core.action.labAction.WaitForDeviceBootAction;
import ca.cyberseclab.avp.core.serviceLogger.PortServiceLogger;
import ca.cyberseclab.avp.core.serviceLogger.ServiceLogger;
import ca.cyberseclab.avp.tasks.AbstractTask;
import ca.cyberseclab.avp.tasks.NavigateToURLTask;
import ca.cyberseclab.avp.tasks.PhoneCallTask;
import ca.cyberseclab.avp.tasks.ReceiveSmsTask;
import ca.cyberseclab.avp.tasks.SendSmsTask;
import ca.cyberseclab.avp.tasks.StartRecordServiceTask;
import ca.cyberseclab.avp.tasks.StopRecordServiceTask;
import ca.cyberseclab.avp.util.AndroidManifest;
import ca.cyberseclab.avp.util.PsParser;
import ca.cyberseclab.avp.util.command.Command;
import ca.cyberseclab.avp.util.command.CommandLineWrapper;
import ca.cyberseclab.avp.util.command.CommandResult;
import ca.cyberseclab.avp.util.logger.LoggerUtil;
import ca.cyberseclab.avp.util.setting.Settings;
import ca.cyberseclab.avp.util.setting.SettingsException;
import ch.qos.logback.classic.Logger;

/**
 * @author sfrenette
 */
public abstract class ADBControllableDevice extends AbstractDevice
{
	private boolean isAdbShellRooted = false;

	public ADBControllableDevice(final String path)
	{
		super(path);
	}

	protected final String getADBPath()
	{
		return Settings.get(Settings.ADB_PATH_KEY);
	}

	//<lafrancef>
	/**
	 * @return A command already setup to execute ADB on the current device.
	 */
	protected final Command getAdbCommand()
	{
		return new Command(this.getADBPath())
		.addParameter("-s", this.systemID);
	}

	/**
	 * @return A command already setup to execute a shell command through ADB on
	 *         the current device.
	 */
	protected final Command getAdbShell()
	{
		return this.getAdbCommand().addParameter("shell");
	}

	/**
	 * Obtains root privileges for ADB commands executed on this device.
	 *
	 * @throws ActionExecutionException
	 */
	protected void rootAdbShell() throws ActionExecutionException
	{
		final Command root = this.getAdbCommand()
						.addParameter("root");
		try
		{
			CommandLineWrapper.executeBlockingCommand(root);
			this.isAdbShellRooted = true;
		}
		catch (final IOException e)
		{
			throw new ActionExecutionException(e);
		}
	}

	/**
	 * @return Whether adbd has root privileges.
	 */
	protected final boolean isAdbShellRooted()
	{
		return this.isAdbShellRooted;
	}

	//</lafrancef>

	/**
	 * Asks the device directly for his status instead of relying on our status system.
	 * Also changes the device's status in our system depending on the request result.
	 *
	 * @return the current device's status
	 */
	public Status getStatusProperty()
	{
		// This shell command returns the device's boot animation status
		// Response "stopped" : booting has stopped, so the device is online
		// Response "running" : booting is running, so the device is booting
		// Response "error..." : cannot access device, so the device stopped
		try
		{
			final Command command = this.getAdbShell()
							.addParameter("getprop", "init.svc.bootanim");

			final String commandResult = CommandLineWrapper.executeBlockingCommand(command);
			this.getLogger().debug("Device boot animation status: " + commandResult);
			//If it's empty, the property might not have been set yet.
			if (commandResult.contains("running") || commandResult.isEmpty())
			{
				this.status = Status.BOOTING;
			}
			else if (commandResult.contains("stopped"))
			{
				this.status = Status.RUNNING;
			}
			else if (commandResult.contains("error"))
			{
				this.status = Status.STOPPED;
			}
			else
			{
				// Should never happen
				this.status = Status.ERROR;
				this.getLogger().error("Unknown error when getting device status.");
			}
		}
		catch (final IOException | SettingsException e)
		{
			this.status = Status.ERROR;
			this.getLogger().error(e.getMessage());
		}
		return this.status;
	}

	@Override
	protected void executeSpecific(final WaitForDeviceBootAction action) throws ActionExecutionException
	{
		int elapsedTime = 0;

		// while device is booting or is still stopped
		Status status;
		while (((status = this.getStatusProperty()) == Status.BOOTING) || (status == Status.STOPPED))
		{
			try
			{
				final long statusCheckInterval = action.checkStatusIntervalTimeMS();
				Thread.sleep(statusCheckInterval);
				elapsedTime += statusCheckInterval;

				if (elapsedTime > action.checkStatusTimeoutTimeMS())
				{
					final String errorMessage = "WaitForDeviceBootAction's timeout has been reached.";
					this.getLogger().error(errorMessage);
					throw new ActionTimeoutException(errorMessage);
				}
			}
			catch (final InterruptedException e)
			{
				// just continue lab
				final String errorMessage = "WaitForDeviceBootAction's sleep has been interrupted.";
				this.getLogger().error(errorMessage);
			}
		}

		try
		{
			Thread.sleep(5000);
			final Command command = this.getAdbShell().addParameter("input", "keyevent", "82");
			CommandLineWrapper.executeNonblockingCommand(command);
		}
		catch (final IOException | InterruptedException e)
		{
			final String errorMessage = "Couldn't unlock screen at boot.";
			this.getLogger().error(errorMessage);
		}
	}

	@Override
	protected void executeSpecific(final StartRecordProcesses action) throws ActionExecutionException
	{
		try
		{
			final Logger logger = LoggerUtil.getLogger("devices.processes." + this.scenarioID);
			final Command command = this.getAdbShell()
							.addParameter("top")
							.setLogger(logger);

			CommandLineWrapper.executeNonblockingCommand(command);
		}
		catch (final IOException e)
		{
			final String errorMessage = String.format("Couldn't start recording processes on device %s." +
							"\nError description: %s.", this.scenarioID, e.getMessage());
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage);
		}
	}

	@Override
	protected void executeSpecific(final StartMonkeyAction action) throws ActionExecutionException
	{
		try
		{
			// adb -s emulator-5554 shell monkey [-p my.package.name] 500
			final Command command = this.getAdbShell().addParameter("monkey");
			String packageName;
			if (action.hasPackageName())
			{
				packageName = action.getPackageName();
			}
			else
			{
				final String localApk = action.getLocalPathToApk();
				final AndroidManifest mani = new AndroidManifest(localApk);
				packageName = mani.getPackageName();
			}

			if (!packageName.isEmpty())
			{
				command.addParameter("-p", packageName);
			}
			command.addParameter(String.valueOf(action.getNumberOfEvents()));

			CommandLineWrapper.executeNonblockingCommand(command);
		}
		catch (final IOException e)
		{
			final String errorMessage = String.format("Couldn't start monkey for package %s with %s events on AVD %s." +
							"\nError description: %s", action.getPackageName(), action.getNumberOfEvents(), this.scenarioID, e.getMessage());
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage, e);
		}
	}

	@Override
	protected void executeSpecific(final PullFileAction action) throws ActionExecutionException
	{
		this.pullFile(action.getRemoteFilePath(), action.getLocalFilePath(), false);
	}

	@Override
	protected void executeSpecific(final PushFileAction action) throws ActionExecutionException
	{
		this.pushFile(action.getRemoteFilePath(), action.getLocalFilePath(), false);
	}

	protected void pullFile(final String remote, final String local, final boolean blocking) throws ActionExecutionException
	{
		try
		{
			final Command command = this.getAdbCommand()
							.addParameter("pull", remote, local);

			if (blocking)
			{
				CommandLineWrapper.executeBlockingCommand(command);
			}
			else
			{
				CommandLineWrapper.executeNonblockingCommand(command);
			}
		}
		catch (IOException | SettingsException e)
		{
			final String errorMessage = String.format("Couldn't pull file %s to destination %s on AVD %s." +
							"\nError description: %s", remote, local, this.scenarioID, e.getMessage());
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage, e);
		}
	}

	protected void pushFile(final String remote, final String local, final boolean blocking) throws ActionExecutionException
	{
		try
		{
			final Command command = this.getAdbCommand()
							.addParameter("push", local, remote);

			if (blocking)
			{
				CommandLineWrapper.executeBlockingCommand(command);
			}
			else
			{
				CommandLineWrapper.executeNonblockingCommand(command);
			}
		}
		catch (IOException | SettingsException e)
		{
			final String errorMessage = String.format("Couldn't push file %s to destination %s on AVD %s." +
							"\nError description: %s", local, remote, this.scenarioID, e.getMessage());
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage, e);
		}
	}

	@Override
	protected void executeSpecific(final InstallAPPAction action) throws ActionExecutionException
	{
		try
		{
			final Command command = this.getAdbCommand().addParameter("install");

			if (action.getReinstall())
			{
				command.addParameter("-r");
			}

			command.addParameter(action.getlocalPathToAPK());
			CommandLineWrapper.executeBlockingCommand(command);
		}
		catch (IOException | SettingsException e)
		{
			final String errorMessage = String.format("Couldn't install file %s on AVD %s." +
							"\nError description: %s", action.getlocalPathToAPK(), this.scenarioID, e.getMessage());
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage, e);
		}
	}

	@Override
	protected void executeSpecific(final StartAPPAction action) throws ActionExecutionException
	{
		final String localPathToAPK = action.getLocalPathToAPK();
		try
		{
			final AndroidManifest applicationManifest = new AndroidManifest(localPathToAPK);

			final Command command = this.getAdbShell()
							.addParameter("am", "start")
							.addParameter("-n", applicationManifest.getPackageName() + "/" + applicationManifest.getMainActivityName());
			CommandLineWrapper.executeNonblockingCommand(command);
		}
		catch (final IOException e)
		{
			final String errorMessage = String.format("Couldn't start application APK at path: %s." +
							"\nError message: %s", localPathToAPK, e.getMessage());
			LoggerUtil.getLogger("error").error(errorMessage);
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage, e);
		}

	}

	@Override
	protected void executeSpecific(final ShellCommandAction action) throws ActionExecutionException
	{
		try
		{
			final Command command = this.getAdbShell().addParameter(action.getCommand().toString());
			CommandLineWrapper.executeNonblockingCommand(command);
		}
		catch (IOException | SettingsException e)
		{
			final String errorMessage = String.format("Couldn't execute command %s on AVD %s." +
							"\nError description: %s", action.getCommand().toString(), this.scenarioID, e.getMessage());
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage, e);
		}
	}

	@Override
	public CommandResult recordLogcat(final String filterParams, final Logger logger) throws IOException
	{
		final Command command = this.getAdbCommand()
						.addParameter("logcat")
						.addParameter(filterParams)
						.setLogger(logger);

		return CommandLineWrapper.executeNonblockingCommand(command);
	}

	protected void recordService(final String service, final boolean start) throws ActionExecutionException
	{
		try
		{
			final AbstractTask task;
			if (start)
			{
				final PortServiceLogger serviceLogger = PortServiceLogger.getInstance();
				if (!serviceLogger.isStarted())
				{
					serviceLogger.startLogging();
				}
				task = new StartRecordServiceTask(String.valueOf(serviceLogger.getPort()), service);
			}
			else
			{
				task = new StopRecordServiceTask(service);
			}

			this.dispatchTask(task);
		}
		catch (final SocketException e)
		{
			throw new ActionExecutionException(e.getMessage(), e);
		}
	}

	@Override
	public void executeSpecific(final StartRecordSMSAction action) throws ActionExecutionException
	{
		this.recordService(ServiceLogger.SMS_STRING, true);
	}

	@Override
	public void executeSpecific(final StartRecordCallAction action) throws ActionExecutionException
	{
		this.recordService(ServiceLogger.CALL_STRING, true);
	}

	@Override
	public void executeSpecific(final StopRecordSMSAction action) throws ActionExecutionException
	{
		this.recordService(ServiceLogger.SMS_STRING, false);
	}

	@Override
	public void executeSpecific(final StopRecordCallAction action) throws ActionExecutionException
	{
		this.recordService(ServiceLogger.CALL_STRING, false);
	}

	@Override
	public void executeSpecific(final StartStraceAction action) throws ActionExecutionException
	{
		try
		{
			final String psResult = this.getPS();
			final PsParser psParser = new PsParser(psResult);
			final String processPID = psParser.getPIDFromProcessName(action.getProcessName());

			final Logger logger = LoggerUtil.getLogger("strace." + action.getProcessName());

			this.storedResults.put("strace_" + action.getProcessName(),
							this.startStrace(processPID, logger));
		}
		catch (final Exception e)
		{
			final String errorMessage = String.format("Couldn't start strace for process %s on AVD %s." +
							"\nError description: %s", action.getProcessName(), this.scenarioID, e.getMessage());
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage, e);
		}
	}

	private String getPS() throws IOException
	{
		final Command getPIDCommand = this.getAdbShell()
						.addParameter("ps");

		final String psResult = CommandLineWrapper.executeBlockingCommand(getPIDCommand);
		return psResult;
	}

	private CommandResult startStrace(final String processPID, final Logger logger) throws IOException
	{
		final Command straceCommand = this.getAdbShell()
						.addParameter("strace", "-p", processPID)
						.setLogger(logger);

		return CommandLineWrapper.executeNonblockingCommand(straceCommand);
	}

	@Override
	public void executeSpecific(final StopStraceAction action) throws ActionExecutionException
	{
		final String key = "strace_" + action.getProcessName();
		if (this.storedResults.containsKey(key))
		{
			this.storedResults.remove(key).stop();
		}
	}

	@Override
	public void executeSpecific(final SendSMSToNumberAction action) throws ActionExecutionException
	{
		final SendSmsTask task = new SendSmsTask(action.getToNumber(), action.getMessage());
		this.dispatchTask(task);
	}

	@Override
	public void executeSpecific(final CallNumberAction action) throws ActionExecutionException
	{
		final PhoneCallTask task = new PhoneCallTask(action.getToNumber());
		this.dispatchTask(task);
	}

	@Override
	public void executeSpecific(final NavigateToURLAction action) throws ActionExecutionException
	{
		final NavigateToURLTask task = new NavigateToURLTask(action.getURL());
		this.dispatchTask(task);
	}

	//<lafrancef>
	@Override
	public void executeSpecific(final StartRecordNetworkTrafficAction action) throws ActionExecutionException
	{
		if (!this.isAdbShellRooted())
		{
			this.rootAdbShell();
		}

		try
		{
			//Check if the "tcpdump" program is available
			final Command checkTcpDump = this.getAdbShell()
							.addParameter("ls", "/system/xbin");

			final String checkTcpDumpResult = CommandLineWrapper.executeBlockingCommand(checkTcpDump);
			if (!checkTcpDumpResult.contains("tcpdump"))
			{
				//Maybe we should try to install it
				throw new UnsupportedActionException("This device does not support network traffic recording.");
			}

			String networkDumpFile = "/sdcard/network";
			final Command startTcpDump = this.getAdbShell()
							.addParameter("tcpdump")
							.addParameter("-s", "2048"); //Maximum number of bytes captured per packet

			final String interf = action.getTargetInterface();
			if (!interf.isEmpty())
			{
				startTcpDump.addParameter("-i", interf); //Capture this interface.
				networkDumpFile += "_" + interf;
			}

			networkDumpFile += ".pcap";
			startTcpDump.addParameter("-w", networkDumpFile); //Write data into this file

			final CommandResult tcpdump = CommandLineWrapper.executeNonblockingCommand(startTcpDump);
			this.storedResults.put("tcpdump_" + interf, tcpdump);
		}
		catch (final IOException e)
		{
			final String errorMessage = "Couldn't start recording network traffic";
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage, e.getCause());
		}

	}

	@Override
	public void executeSpecific(final StopRecordNetworkTrafficAction action) throws ActionExecutionException
	{
		final String interf = action.getTargetInterface();
		final String key = "tcpdump_" + interf;

		if (this.storedResults.containsKey(key))
		{
			try
			{
				//Terminate tcpdump and remove it from the stored results
				this.storedResults.remove(key).stop();

				String remoteNetworkDumpFile = "/sdcard/network";
				String localNetworkDumpFile = LabManagerMaster.getScenarioResultFolderPath() +
								"\\network\\" + this.scenarioID;
				if (!interf.isEmpty())
				{
					remoteNetworkDumpFile += "_" + interf;
					localNetworkDumpFile += "_" + interf;
				}
				remoteNetworkDumpFile += ".pcap";
				localNetworkDumpFile += ".pcap";

				//Obtain the file and place it in the logs
				this.pullFile(remoteNetworkDumpFile, localNetworkDumpFile, true);

				//Remove it from the device
				final Command removeDump = this.getAdbShell()
								.addParameter("rm", remoteNetworkDumpFile);

				CommandLineWrapper.executeNonblockingCommand(removeDump);
			}
			catch (final IOException e)
			{
				final String errorMessage = "Couldn't stop recording network traffic";
				this.getLogger().error(errorMessage);
				throw new ActionExecutionException(errorMessage, e.getCause());
			}
		}
	}

	/* (non-Javadoc)
	 * @see ca.thalesgroup.vl4m.core.device.AbstractDevice#executeSpecific(ca.thalesgroup.vl4m.core.action.labAction.WaitForAppInstallAction)
	 */
	@Override
	public void executeSpecific(final WaitForAppInstallAction action) throws ActionExecutionException
	{
		try
		{
			final AndroidManifest manifest = new AndroidManifest(action.getLocalPathToAPK());
			final String name = manifest.getPackageName();
			final Command isPackageInstalled = this.getAdbShell()
							.addParameter("pm")
							.addParameter("list", "packages", name);

			String result = "";
			boolean keepGoing = true;
			while (keepGoing)
			{
				result = CommandLineWrapper.executeBlockingCommand(isPackageInstalled);
				if (result.isEmpty())
				{
					this.getLogger().debug("Application " + name + " is not installed");
					Thread.sleep(500);
				}
				else
				{
					this.getLogger().debug("Application " + name + " is installed");
					keepGoing = false;
				}
			}
		}
		catch (final IOException e)
		{
			final String message = "WaitForAppInstallApplication action failed: " + e.getMessage();
			this.getLogger().error(message);
			throw new ActionExecutionException(message, e);
		}
		catch (final InterruptedException e)
		{
			//This should not happen. Do nothing special.
		}
	}

	@Override
	public void executeSpecific(final LabSendSMSToDeviceAction action) throws ActionExecutionException
	{
		//This method of doing things allows devices that would normally be unable to receive SMS messages
		//to receive them.
		final ReceiveSmsTask rst = new ReceiveSmsTask(action.getFromNumber(), action.getMessage());
		this.dispatchTask(rst);
	}

	//</lafrancef>

	protected void dispatchTask(final AbstractTask task) throws ActionExecutionException
	{
		try
		{
			// this command accesses the device's shell and broadcasts our execute task intent
			// note: the parameter -f 32 forces our application on the device to wake up and catch the intent
			final Command command = this.getAdbShell()
							.addParameter("am", "broadcast")
							.addParameter("-a", "ca.thalesgroup.vl4m.EXEC_TASK")
							.addParameter("-f", "32");

			// building the intent's params
			// note: every key/value must be surrounded by double quotes
			final String taskNameKey = "\"" + AbstractTask.TASK_NAME_PARAM + "\"";
			final String taskNameValue = "\"" + task.getClass().getName() + "\"";
			command.addParameter("--es", taskNameKey, taskNameValue);

			for (final Entry<String, String> param : task)
			{
				final String key = "\"" + param.getKey() + "\"";
				final String value = "\"" + param.getValue() + "\"";
				command.addParameter("--es", key, value);
			}

			CommandLineWrapper.executeNonblockingCommand(command);
		}
		catch (IOException | SettingsException e)
		{
			final String errorMessage = String.format("Couldn't dispatch task %s." +
							"\nError description: %s.",
							"\nTask parameters: %s.",
							task.getClass().getSimpleName(),
							e.getMessage(),
							task.toString());
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage, e.getCause());
		}
	}

}

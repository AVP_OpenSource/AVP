/**
 * 
 */
package ca.cyberseclab.avp.core.device;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import ca.cyberseclab.avp.core.action.deviceAction.CallNumberAction;
import ca.cyberseclab.avp.core.action.deviceAction.NavigateToURLAction;
import ca.cyberseclab.avp.core.action.deviceAction.SendSMSToNumberAction;
import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.action.exception.UnsupportedActionException;
import ca.cyberseclab.avp.core.action.labAction.InstallAPPAction;
import ca.cyberseclab.avp.core.action.labAction.LabCallDeviceAction;
import ca.cyberseclab.avp.core.action.labAction.LabSendSMSToDeviceAction;
import ca.cyberseclab.avp.core.action.labAction.PullFileAction;
import ca.cyberseclab.avp.core.action.labAction.PushFileAction;
import ca.cyberseclab.avp.core.action.labAction.ShellCommandAction;
import ca.cyberseclab.avp.core.action.labAction.StartAPPAction;
import ca.cyberseclab.avp.core.action.labAction.StartAVDAction;
import ca.cyberseclab.avp.core.action.labAction.StartMonkeyAction;
import ca.cyberseclab.avp.core.action.labAction.StartRecordCallAction;
import ca.cyberseclab.avp.core.action.labAction.StartRecordNetworkTrafficAction;
import ca.cyberseclab.avp.core.action.labAction.StartRecordProcesses;
import ca.cyberseclab.avp.core.action.labAction.StartRecordSMSAction;
import ca.cyberseclab.avp.core.action.labAction.StartRecordTaintDroid;
import ca.cyberseclab.avp.core.action.labAction.StartStraceAction;
import ca.cyberseclab.avp.core.action.labAction.StopAVDAction;
import ca.cyberseclab.avp.core.action.labAction.StopRecordCallAction;
import ca.cyberseclab.avp.core.action.labAction.StopRecordNetworkTrafficAction;
import ca.cyberseclab.avp.core.action.labAction.StopRecordSMSAction;
import ca.cyberseclab.avp.core.action.labAction.StopRecordTaintDroid;
import ca.cyberseclab.avp.core.action.labAction.StopStraceAction;
import ca.cyberseclab.avp.core.action.labAction.WaitForAppInstallAction;
import ca.cyberseclab.avp.core.action.labAction.WaitForDeviceBootAction;
import ca.cyberseclab.avp.util.command.CommandResult;
import ca.cyberseclab.avp.util.logger.LoggerUtil;
import ch.qos.logback.classic.Logger;

/**
 * @author sfrenette
 */
public abstract class AbstractDevice
{
	public enum Status
	{
		STOPPED,
		BOOTING,
		RUNNING,
		ERROR
	}

	public static final String TECH_GOOGLE = "google";
	public static final String TECH_GOOGLE_MODIFIED = "google_modified";
	public static final String TECH_TAINTDROID = "taintdroid";
	public static final String TECH_GENYMOTION = "genymotion";
	public static final String TECH_PHYSICAL = "physical";

	protected Status status;
	protected String scenarioID;
	protected String name;

	// TODO_Frank Is system ID generic... how to deal with it (set vs construct, move to subclass ?)
	// Should be an object ?
	protected String systemID = null;

	protected Map<String, CommandResult> storedResults = new HashMap<String, CommandResult>();

	public AbstractDevice(final String path)
	{
		this.name = path;
		this.status = Status.STOPPED;
	}

	public void assignID(final String ID)
	{
		this.scenarioID = ID;
	}

	//<lafrancef>
	public void assignSystemId(final String systemId)
	{
		if (this.systemID == null)
		{
			this.systemID = systemId;
		}
		else
		{
			throw new IllegalStateException();
		}
	}

	//</lafrancef>

	// StartAVD
	public final void execute(final StartAVDAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
		this.status = Status.BOOTING;
	}

	protected abstract void executeSpecific(final StartAVDAction action) throws ActionExecutionException;

	// WaitForDeviceBoot
	public final void execute(final WaitForDeviceBootAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
		this.status = Status.RUNNING;
		try
		{
			Thread.sleep(2000);
			this.recordLogcat();
		}
		catch (final Exception e)
		{
			throw new ActionExecutionException("Couldn't start logcat dump.");
		}
	}

	protected abstract void executeSpecific(final WaitForDeviceBootAction action) throws ActionExecutionException;

	// StartMonkey
	public final void execute(final StartMonkeyAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	protected abstract void executeSpecific(final StartMonkeyAction action) throws ActionExecutionException;

	// StopAVD
	public final void execute(final StopAVDAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
		this.status = Status.STOPPED;

		for (final Map.Entry<String, CommandResult> result : this.storedResults.entrySet())
		{
			if (result.getValue().isRunning())
			{
				result.getValue().stop();
			}
		}
	}

	protected abstract void executeSpecific(final StopAVDAction action) throws ActionExecutionException;

	// PullFile
	public final void execute(final PullFileAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	protected abstract void executeSpecific(final PullFileAction action) throws ActionExecutionException;

	// PushFile
	public final void execute(final PushFileAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	protected abstract void executeSpecific(final PushFileAction action) throws ActionExecutionException;

	// InstallAPP
	public final void execute(final InstallAPPAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	protected abstract void executeSpecific(final InstallAPPAction action) throws ActionExecutionException;

	// StartAPP
	public final void execute(final StartAPPAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	protected abstract void executeSpecific(final StartAPPAction action) throws ActionExecutionException;

	// ShellCommand
	public final void execute(final ShellCommandAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	protected abstract void executeSpecific(final ShellCommandAction action) throws ActionExecutionException;

	// StartRecordProcesses
	public final void execute(final StartRecordProcesses action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	protected abstract void executeSpecific(final StartRecordProcesses action) throws ActionExecutionException;

	// StartRecordNetworkTraffic
	public final void execute(final StartRecordNetworkTrafficAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	public abstract void executeSpecific(final StartRecordNetworkTrafficAction action) throws ActionExecutionException;

	// StopRecordNetworkTraffic
	public final void execute(final StopRecordNetworkTrafficAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	public abstract void executeSpecific(final StopRecordNetworkTrafficAction action) throws ActionExecutionException;

	// StartRecordSMS
	public final void execute(final StartRecordSMSAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	public abstract void executeSpecific(final StartRecordSMSAction action) throws ActionExecutionException;

	// StartRecordCall
	public final void execute(final StartRecordCallAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	public abstract void executeSpecific(final StartRecordCallAction action) throws ActionExecutionException;

	// StopRecordSMS
	public final void execute(final StopRecordSMSAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	public abstract void executeSpecific(final StopRecordSMSAction action) throws ActionExecutionException;

	// StopRecordCall
	public final void execute(final StopRecordCallAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	public abstract void executeSpecific(final StopRecordCallAction action) throws ActionExecutionException;

	// StartStrace
	public final void execute(final StartStraceAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	public abstract void executeSpecific(final StartStraceAction action) throws ActionExecutionException;

	// StopStrace
	public final void execute(final StopStraceAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	public abstract void executeSpecific(final StopStraceAction action) throws ActionExecutionException;

	// LabSendSMSToDevice
	public final void execute(final LabSendSMSToDeviceAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	public abstract void executeSpecific(final LabSendSMSToDeviceAction action) throws ActionExecutionException;

	// LabCallDevice
	public final void execute(final LabCallDeviceAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	public abstract void executeSpecific(final LabCallDeviceAction action) throws ActionExecutionException;

	// SendSMSToNumber
	public final void execute(final SendSMSToNumberAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	public abstract void executeSpecific(final SendSMSToNumberAction action) throws ActionExecutionException;

	// CallNumber
	public final void execute(final CallNumberAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	public abstract void executeSpecific(final CallNumberAction action) throws ActionExecutionException;

	// NavigateToURL
	public final void execute(final NavigateToURLAction action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	public abstract void executeSpecific(final NavigateToURLAction action) throws ActionExecutionException;

	//StartRecordTaintDroid
	public final void execute(final StartRecordTaintDroid action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	public void executeSpecific(final StartRecordTaintDroid action) throws ActionExecutionException
	{
		throw new UnsupportedActionException("Normal devices do not support TaintDroid");
	}

	//StopRecordTaintDroid
	public final void execute(final StopRecordTaintDroid action) throws ActionExecutionException
	{
		this.executeSpecific(action);
	}

	public void executeSpecific(final StopRecordTaintDroid action) throws ActionExecutionException
	{
		throw new UnsupportedActionException("Normal devices do not support TaintDroid");
	}

	//WaitForAppInstall
	public final void execute(final WaitForAppInstallAction waitForAppInstallAction) throws ActionExecutionException
	{
		this.executeSpecific(waitForAppInstallAction);
	}

	public abstract void executeSpecific(WaitForAppInstallAction waitForAppInstallAction) throws ActionExecutionException;

	/**
	 * Records the LogCat on this device without filters and with the default logger.
	 * 
	 * @throws IOException
	 */
	public void recordLogcat() throws IOException
	{
		this.recordLogcat("");
	}

	/**
	 * Records the LogCat on this device with a particular filter and with
	 * the default logger
	 * 
	 * @param filterParams The filtering parameters to apply to LogCat
	 * @throws IOException
	 */
	public void recordLogcat(final String filterParams) throws IOException
	{
		final String loggerName = "logcat." + this.scenarioID;
		final Logger logger = LoggerUtil.getLogger(loggerName, true);

		this.recordLogcat(filterParams, logger);
	}

	/**
	 * Records the LogCat on this device with a particular filter and a custom logger.
	 * 
	 * @param filterParams The filtering parameters to apply to LogCat
	 * @param logger A custom logger
	 * @return A CommandResult containing the launched logcat process
	 * @throws IOException
	 */
	public abstract CommandResult recordLogcat(String filterParams, Logger logger) throws IOException;

	public Logger getLogger()
	{
		final Logger logger = LoggerUtil.getLogger("devices." + this.getClass().getSimpleName() + "." + this.scenarioID, true);
		return logger;
	}

	public String getName()
	{
		return this.name;
	}

	public String getScenarioID()
	{
		return this.scenarioID;
	}

	public Status getStatus()
	{
		return this.status;
	}
}

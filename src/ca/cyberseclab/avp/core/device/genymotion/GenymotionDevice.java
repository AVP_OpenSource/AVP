/**
 * 
 */
package ca.cyberseclab.avp.core.device.genymotion;

import java.io.IOException;

import ca.cyberseclab.avp.core.action.deviceAction.CallNumberAction;
import ca.cyberseclab.avp.core.action.deviceAction.SendSMSToNumberAction;
import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.action.exception.CannotStartDeviceException;
import ca.cyberseclab.avp.core.action.exception.UnsupportedActionException;
import ca.cyberseclab.avp.core.action.labAction.LabCallDeviceAction;
import ca.cyberseclab.avp.core.action.labAction.StartAVDAction;
import ca.cyberseclab.avp.core.action.labAction.StartRecordCallAction;
import ca.cyberseclab.avp.core.action.labAction.StartRecordSMSAction;
import ca.cyberseclab.avp.core.action.labAction.StopAVDAction;
import ca.cyberseclab.avp.core.action.labAction.StopRecordCallAction;
import ca.cyberseclab.avp.core.action.labAction.StopRecordSMSAction;
import ca.cyberseclab.avp.core.device.ADBControllableDevice;
import ca.cyberseclab.avp.util.command.Command;
import ca.cyberseclab.avp.util.command.CommandLineWrapper;
import ca.cyberseclab.avp.util.setting.Settings;
import ca.cyberseclab.avp.util.setting.SettingsException;

/**
 * @author lafrancef
 */
public class GenymotionDevice extends ADBControllableDevice
{
	public GenymotionDevice(final String path)
	{
		super(path);
	}

	@Override
	protected void executeSpecific(final StartAVDAction action) throws ActionExecutionException
	{
		try
		{
			final Command start = new Command(Settings.get(Settings.GENYMOTION_PATH_KEY))
							.addParameter("--vm-name", this.name);
			CommandLineWrapper.executeNonblockingCommand(start);
		}
		catch (IOException | SettingsException e)
		{
			final String errorMessage = String.format("Couldn't start Genymotion device %s." +
							"\nError description: %s", this.scenarioID, e.getMessage());
			this.getLogger().error(errorMessage);
			throw new CannotStartDeviceException(errorMessage, e);
		}
	}

	@Override
	protected void executeSpecific(final StopAVDAction action) throws ActionExecutionException
	{
		final Command stop = new Command(Settings.get(Settings.VBOX_MANAGE_PATH_KEY))
						.addParameter("controlvm", this.name)
						.addParameter("poweroff");
		try
		{
			CommandLineWrapper.executeNonblockingCommand(stop);
		}
		catch (final IOException e)
		{
			final String errorMessage = String.format("Couldn't stop Genymotion device %s." +
							"\nError description: %s", this.name, e.getMessage());
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage, e);
		}
	}

	/* (non-Javadoc)
	 * @see ca.thalesgroup.vl4m.core.device.ADBControllableDevice#rootAdbShell()
	 */
	@Override
	protected void rootAdbShell() throws ActionExecutionException
	{
		final Command su = this.getAdbShell().addParameter("su");
		try
		{
			CommandLineWrapper.executeNonblockingCommand(su);
		}
		catch (final IOException e)
		{
			final String errorMessage = String.format("Couldn't get superuser rights for Genymotion device %s." +
							"\nError description: %s", this.name, e.getMessage());
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage, e);
		}
	}

	//==============================
	//     Unsupported actions
	//==============================

	@Override
	public void executeSpecific(final StartRecordCallAction action) throws ActionExecutionException
	{
		throw new UnsupportedActionException("Genymotion devices cannot make calls.");
	}

	@Override
	public void executeSpecific(final StartRecordSMSAction action) throws ActionExecutionException
	{
		throw new UnsupportedActionException("Genymotion devices cannot send SMS messages.");
	}

	@Override
	public void executeSpecific(final StopRecordCallAction action) throws ActionExecutionException
	{
		throw new UnsupportedActionException("Genymotion devices cannot make calls.");
	}

	@Override
	public void executeSpecific(final StopRecordSMSAction action) throws ActionExecutionException
	{
		throw new UnsupportedActionException("Genymotion devices cannot send SMS messages.");
	}

	@Override
	public void executeSpecific(final CallNumberAction action) throws ActionExecutionException
	{
		throw new UnsupportedActionException("Genymotion devices cannot make calls.");
	}

	@Override
	public void executeSpecific(final SendSMSToNumberAction action) throws ActionExecutionException
	{
		throw new UnsupportedActionException("Genymotion devices cannot send SMS messages.");
	}

	@Override
	public void executeSpecific(final LabCallDeviceAction action) throws ActionExecutionException
	{
		throw new UnsupportedActionException("Genymotion devices cannot simulate receiving calls.");
	}
}

/**
 * 
 */
package ca.cyberseclab.avp.core.device.google;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.serviceLogger.FileServiceLogger;
import ca.cyberseclab.avp.util.setting.Settings;

/**
 * @author sfrenette
 */
public class CustomGoogleDevice extends GoogleDevice
{

	public CustomGoogleDevice(final String path)
	{
		super(path);
	}

	@Override
	protected String getEmulatorPath()
	{
		return Settings.get(Settings.CUSTOM_EMULATOR_PATH_KEY);
	}

	@Override
	protected void recordService(final String service, final boolean start) throws ActionExecutionException
	{
		try
		{
			final String fileName = this.getPortAsString() + "-" + service;
			final Path file = Paths.get(FileServiceLogger.REQUESTS_FOLDER_PATH, fileName);

			if (start)
			{
				final FileServiceLogger serviceLogger = FileServiceLogger.getInstance();
				if (!serviceLogger.isStarted())
				{
					serviceLogger.startLogging();
				}

				file.toFile().createNewFile();
			}
			else
			{
				file.toFile().delete();
			}
		}
		catch (final IOException e)
		{
			throw new ActionExecutionException(e.getMessage(), e);
		}
	}
}

/**
 * 
 */
package ca.cyberseclab.avp.core.device.google;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.net.telnet.TelnetClient;

import ca.cyberseclab.avp.core.LabManagerMaster;
import ca.cyberseclab.avp.core.PhoneBook;
import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.action.exception.CannotStartDeviceException;
import ca.cyberseclab.avp.core.action.labAction.LabCallDeviceAction;
import ca.cyberseclab.avp.core.action.labAction.LabSendSMSToDeviceAction;
import ca.cyberseclab.avp.core.action.labAction.StartAVDAction;
import ca.cyberseclab.avp.core.action.labAction.StartRecordNetworkTrafficAction;
import ca.cyberseclab.avp.core.action.labAction.StopAVDAction;
import ca.cyberseclab.avp.core.action.labAction.StopRecordNetworkTrafficAction;
import ca.cyberseclab.avp.core.device.ADBControllableDevice;
import ca.cyberseclab.avp.util.DateStringUtil;
import ca.cyberseclab.avp.util.ZipUtil;
import ca.cyberseclab.avp.util.command.Command;
import ca.cyberseclab.avp.util.command.CommandLineWrapper;
import ca.cyberseclab.avp.util.command.CommandResult;
import ca.cyberseclab.avp.util.logger.LoggerUtil;
import ca.cyberseclab.avp.util.setting.Settings;
import ca.cyberseclab.avp.util.setting.SettingsException;

/**
 * @author sfrenette
 */
public class GoogleDevice extends ADBControllableDevice
{
	private static final int MIN_PORT = 5554;
	private static final int MAX_PORT = 5584;

	protected CommandResult emulatorProcess;
	protected int port;

	protected static int getNextAvailablePort() throws CannotStartDeviceException
	{
		for (int port = GoogleDevice.MIN_PORT; port <= GoogleDevice.MAX_PORT; port = port + 2)
		{
			if (!GoogleDevice.isPortUsed(port))
			{
				if (!GoogleDevice.isPortUsedOutside(port + 1))
				{
					return port;
				}
			}
		}
		throw new CannotStartDeviceException("No port available for the AVD to connect to. Aborting...");
	}

	private static boolean isPortUsed(final int portNumber)
	{
		return (GoogleDevice.isPortUsedByLab(portNumber) || GoogleDevice.isPortUsedOutside(portNumber));
	}

	private static boolean isPortUsedByLab(final int portNumber)
	{
		return PhoneBook.getInstance().containsNumber(String.valueOf(portNumber));
	}

	private static boolean isPortUsedOutside(final int portNumber)
	{
		final TelnetClient telnetClient = new TelnetClient();
		try
		{
			telnetClient.connect("localhost", portNumber);
			telnetClient.disconnect();
			return true;
		}
		catch (final IOException e)
		{
			// if there is no service on the port, we won't be able to establish a TCP connection.
			// We then get an IOException, which mean the port is not used and we can assign it to our device
			return false;
		}
	}

	private boolean persistent = false;

	public GoogleDevice(final String path)
	{
		super(path);
	}

	protected String getPortAsString()
	{
		return String.valueOf(this.port);
	}

	protected String getEmulatorPath()
	{
		return Settings.get(Settings.EMULATOR_PATH_KEY);
	}

	@Override
	public void executeSpecific(final StartAVDAction action) throws CannotStartDeviceException
	{
		this.persistent = action.isPersistent();
		if (!action.isPersistent())
		{
			try
			{
				this.copyAVD();
			}
			catch (final IOException e)
			{
				final String errorMessage = String.format("Couldn't make AVD %s volatile." +
								"\nError description: %s", this.scenarioID, e.getMessage());
				this.getLogger().error(errorMessage);
				throw new CannotStartDeviceException(errorMessage, e);
			}
		}

		try
		{
			if (this.port == 0)
			{
				this.port = GoogleDevice.getNextAvailablePort();
				this.systemID = "emulator-" + this.port;

				PhoneBook.getInstance().put(String.valueOf(this.port), this);
			}

			final Command command = new Command(this.getEmulatorPath())
							.addParameter("-port", this.getPortAsString())
							.addParameter("-avd", this.name)
							// .addParameter("-no-window")
							.addParameter("-no-boot-anim")
							.addParameter("-scale", "0.3");
			//TODO fix the issue with this env var: the emulator will automatically add \.android\avd\ to the provided env variable value.
			//.addEnvironmentVariable("ANDROID_SDK_HOME", Settings.get(Settings.GOOGLE_AVD_PATH_KEY));

			this.emulatorProcess = CommandLineWrapper.executeNonblockingCommand(command);
		}
		catch (IOException | CannotStartDeviceException | SettingsException e)
		{
			final String errorMessage = String.format("Couldn't start AVD %s." +
							"\nError description: %s", this.scenarioID, e.getMessage());
			this.getLogger().error(errorMessage);
			throw new CannotStartDeviceException(errorMessage, e);
		}
	}

	private void copyAVD() throws IOException
	{
		final String AVDPath = Settings.get(Settings.GOOGLE_AVD_PATH_KEY);
		final Path AVDFolderPath = Paths.get(AVDPath, this.name + ".avd");
		final Path zipPath = Paths.get(AVDPath, this.name + ".avd.zip");

		ZipUtil.zipFolder(AVDFolderPath.toFile(), zipPath.toFile());
	}

	@Override
	public void executeSpecific(final StopAVDAction action) throws ActionExecutionException
	{
		if (this.emulatorProcess != null)
		{
			try
			{
				CommandLineWrapper.executeTelnetCommand("localhost", this.getPortAsString(), new Command("kill"));
			}
			catch (final Exception e)
			{
				this.emulatorProcess.stop();
			}
		}

		if (!this.persistent)
		{
			try
			{
				this.restoreAVD();
			}
			catch (final IOException e)
			{
				final String AVDPath = Settings.get(Settings.GOOGLE_AVD_PATH_KEY);
				final Path zipPath = Paths.get(AVDPath, this.name, ".avd", ".zip");

				final String errorMessage = String.format("Couldn't restore AVD %s. Unzip file %s to restore it manually." +
								"\nError description: %s", this.scenarioID, zipPath.toString(), e.getMessage());
				this.getLogger().error(errorMessage);
				LoggerUtil.getLogger("error").error(errorMessage);
			}
		}
	}

	private void restoreAVD() throws IOException
	{
		final String AVDPath = Settings.get(Settings.GOOGLE_AVD_PATH_KEY);
		final Path AVDFolderPath = Paths.get(AVDPath, this.name + ".avd");
		AVDFolderPath.toFile().delete();

		final Path zipPath = Paths.get(AVDPath, this.name + ".avd.zip");
		ZipUtil.unzip(zipPath.toFile());
	}

	@Override
	public void executeSpecific(final StartRecordNetworkTrafficAction action) throws ActionExecutionException
	{
		try
		{
			final Path networkFolderPath = Paths.get(LabManagerMaster.getScenarioResultFolderPath(), "network", "");
			final File networkFolder = networkFolderPath.toFile();
			if (!networkFolder.exists())
			{
				networkFolder.mkdir();
			}
			final Path capturePath = Paths.get(networkFolderPath.toString(), this.scenarioID + "_" + DateStringUtil.get() + ".pcap");
			final Command command = new Command("network")
							.addParameter("capture")
							.addParameter("start", capturePath.toString());
			CommandLineWrapper.executeTelnetCommand("localhost", this.getPortAsString(), command);
		}
		catch (final IOException e)
		{
			final String errorMessage = String.format("Couldn't start to record network traffic on AVD %s." +
							"\nError description: %s", this.scenarioID, e.getMessage());
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage);
		}
	}

	@Override
	public void executeSpecific(final StopRecordNetworkTrafficAction action) throws ActionExecutionException
	{
		try
		{
			final Command command = new Command("network")
							.addParameter("capture", "stop");
			CommandLineWrapper.executeTelnetCommand("localhost", this.getPortAsString(), command);
		}
		catch (final IOException e)
		{
			final String errorMessage = String.format("Couldn't stop to record network traffic on AVD %s." +
							"\nError description: %s", this.scenarioID, e.getMessage());
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage);
		}
	}

	@Override
	public void executeSpecific(final LabSendSMSToDeviceAction action) throws ActionExecutionException
	{
		try
		{
			final Command command = new Command("sms")
							.addParameter("send", action.getFromNumber(), action.getMessage());
			CommandLineWrapper.executeTelnetCommand("localhost", this.getPortAsString(), command);
		}
		catch (final IOException e)
		{
			final String errorMessage = String.format("Couldn't send sms \"%s\" from number %s to AVD %s ." +
							"\nError description: %s", action.getMessage(), action.getFromNumber(), this.scenarioID, e.getMessage());
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage);
		}
	}

	@Override
	public void executeSpecific(final LabCallDeviceAction action) throws ActionExecutionException
	{
		try
		{
			final Command command = new Command("gsm")
							.addParameter("call", action.getFromNumber());
			CommandLineWrapper.executeTelnetCommand("localhost", this.getPortAsString(), command);
		}
		catch (final IOException e)
		{
			final String errorMessage = String.format("Couldn't call AVD %s from number %s." +
							"\nError description: %s", this.scenarioID, action.getFromNumber(), e.getMessage());
			this.getLogger().error(errorMessage);
			throw new ActionExecutionException(errorMessage);
		}
	}

}

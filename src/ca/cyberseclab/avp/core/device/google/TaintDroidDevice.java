/**
 * 
 */
package ca.cyberseclab.avp.core.device.google;

import java.io.IOException;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.action.exception.CannotStartDeviceException;
import ca.cyberseclab.avp.core.action.labAction.StartAVDAction;
import ca.cyberseclab.avp.core.action.labAction.StartRecordTaintDroid;
import ca.cyberseclab.avp.core.action.labAction.StopRecordTaintDroid;
import ca.cyberseclab.avp.util.command.Command;
import ca.cyberseclab.avp.util.command.CommandLineWrapper;
import ca.cyberseclab.avp.util.logger.LoggerUtil;
import ca.cyberseclab.avp.util.setting.Settings;
import ca.cyberseclab.avp.util.setting.SettingsException;
import ch.qos.logback.classic.Logger;

/**
 * @author lafrancef
 */
public class TaintDroidDevice extends GoogleDevice
{
	private static final String TAINTED_KERNEL_FILENAME = "kernel-tdroid";
	private static final String TAINTED_RAMDISK_FILENAME = "ramdisk.img";
	private static final String TAINTED_SYSDISK_FILENAME = "system.img";
	private static final String TAINTED_USRDATA_FILENAME = "userdata.img";

	public TaintDroidDevice(final String path)
	{
		super(path);
	}

	@Override
	public void executeSpecific(final StartAVDAction action) throws CannotStartDeviceException
	{
		try
		{
			this.port = GoogleDevice.getNextAvailablePort();
			this.systemID = "emulator-" + this.port;

			final String taintedImgsPath = Settings.get(Settings.TAINTDROID_IMAGES_PATH_KEY);

			final Command command = new Command(this.getEmulatorPath())
							.addParameter("-port", this.getPortAsString())
							.addParameter("-avd", this.name)
							// .addParameter("-no-window")
							//.addParameter("-no-boot-anim")
							//.addParameter("-scale", "0.3")
							.addParameter(true, "-kernel", taintedImgsPath + "\\" + TaintDroidDevice.TAINTED_KERNEL_FILENAME)
							.addParameter(true, "-system", taintedImgsPath + "\\" + TaintDroidDevice.TAINTED_SYSDISK_FILENAME)
							.addParameter(true, "-ramdisk", taintedImgsPath + "\\" + TaintDroidDevice.TAINTED_RAMDISK_FILENAME)
							.addParameter(true, "-data", taintedImgsPath + "\\" + TaintDroidDevice.TAINTED_USRDATA_FILENAME);

			CommandLineWrapper.executeNonblockingCommand(command);
		}
		catch (IOException | CannotStartDeviceException | SettingsException e)
		{
			final String errorMessage = String.format("Couldn't start AVD %s." +
							"\nError description: %s", this.scenarioID, e.getMessage());
			this.getLogger().error(errorMessage);
			throw new CannotStartDeviceException(errorMessage, e);
		}
	}

	/* (non-Javadoc)
	 * @see ca.thalesgroup.vl4m.core.device.AbstractDevice#executeSpecific(ca.thalesgroup.vl4m.core.action.labAction.StartRecordTaintDroid)
	 */
	@Override
	public void executeSpecific(final StartRecordTaintDroid action) throws ActionExecutionException
	{
		final Logger logger = LoggerUtil.getLogger("services.taintdroid." + this.scenarioID, true);

		try
		{
			this.storedResults.put("taintdroid", this.recordLogcat("*:S TaintLog:W", logger));
		}
		catch (final IOException e)
		{
			final String message = "Couldn't start recording LogCat for TaintDroid: " + e.getMessage();
			this.getLogger().error(message);
			throw new ActionExecutionException(message, e);
		}
	}

	/* (non-Javadoc)
	 * @see ca.thalesgroup.vl4m.core.device.AbstractDevice#executeSpecific(ca.thalesgroup.vl4m.core.action.labAction.StopRecordTaintDroid)
	 */
	@Override
	public void executeSpecific(final StopRecordTaintDroid action) throws ActionExecutionException
	{
		if (this.storedResults.containsKey("taintdroid"))
		{
			this.storedResults.remove("taintdroid").stop();
		}
	}

}

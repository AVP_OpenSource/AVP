/**
 * 
 */
package ca.cyberseclab.avp.core.device.physical;

import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;
import ca.cyberseclab.avp.core.action.exception.UnsupportedActionException;
import ca.cyberseclab.avp.core.action.labAction.LabCallDeviceAction;
import ca.cyberseclab.avp.core.action.labAction.StartAVDAction;
import ca.cyberseclab.avp.core.action.labAction.StopAVDAction;
import ca.cyberseclab.avp.core.device.ADBControllableDevice;

/**
 * @author lafrancef
 */
public class PhysicalDevice extends ADBControllableDevice
{

	public PhysicalDevice(final String path)
	{
		super(path);
	}

	/* (non-Javadoc)
	 * @see ca.thalesgroup.vl4m.core.device.AbstractDevice#executeSpecific(ca.thalesgroup.vl4m.core.action.labAction.StartAVDAction)
	 */
	@Override
	protected void executeSpecific(final StartAVDAction action) throws ActionExecutionException
	{
		//Do nothing.
	}

	/* (non-Javadoc)
	 * @see ca.thalesgroup.vl4m.core.device.AbstractDevice#executeSpecific(ca.thalesgroup.vl4m.core.action.labAction.StopAVDAction)
	 */
	@Override
	protected void executeSpecific(final StopAVDAction action) throws ActionExecutionException
	{
		/* Fred:
		 * A previous version of this function contained code that did shut the device down, but
		 * I removed it. The reason is that as part of the lab clean-up, all devices are stopped.
		 * This causes some problems with physical devices because we can't start them programatically yet.
		 * 
		 * The code of this function simply executed the "reboot -p" adb shell command.
		 */
	}

	//=====================
	// Unsupported actions
	//=====================

	/* (non-Javadoc)
	 * @see ca.thalesgroup.vl4m.core.device.AbstractDevice#executeSpecific(ca.thalesgroup.vl4m.core.action.labAction.LabCallDeviceAction)
	 */
	@Override
	public void executeSpecific(final LabCallDeviceAction action) throws ActionExecutionException
	{
		throw new UnsupportedActionException();
	}

}

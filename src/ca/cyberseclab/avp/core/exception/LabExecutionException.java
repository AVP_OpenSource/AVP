/**
 * 
 */
package ca.cyberseclab.avp.core.exception;

/**
 * @author lafrancef
 */
public class LabExecutionException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4721559152188020732L;

	public LabExecutionException()
	{
	}

	public LabExecutionException(final String message)
	{
		super(message);
	}

	public LabExecutionException(final String message, final Exception e)
	{
		super(message, e);
	}

}

package ca.cyberseclab.avp.core.serviceLogger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import ca.cyberseclab.avp.util.logger.LoggerUtil;

/**
 * The FileServiceLogger watches a folder until there's a new file containing a service event.
 * It then reads the file and logs the event.
 * 
 * @author sfrenette
 */
public class FileServiceLogger extends ServiceLogger
{
	private static FileServiceLogger INSTANCE;

	public static FileServiceLogger getInstance()
	{
		if (FileServiceLogger.INSTANCE == null)
		{
			FileServiceLogger.INSTANCE = new FileServiceLogger();
		}
		return FileServiceLogger.INSTANCE;
	}

	public static boolean isInstanciated()
	{
		return FileServiceLogger.INSTANCE != null;
	}

	public static String ROOT_FOLDER_PATH = "C:\\vl4m\\hijack";
	public static String REQUESTS_FOLDER_PATH = FileServiceLogger.ROOT_FOLDER_PATH + "\\request";
	public static String RESULTS_FOLDER_PATH = FileServiceLogger.ROOT_FOLDER_PATH + "\\result";
	public static String ENVIRONMENT_VAR = "VL4M_ROOT";

	private FileServiceLogger()
	{
		final String environmentPath = this.getEnvironmentPath();
		if (environmentPath != null)
		{
			FileServiceLogger.ROOT_FOLDER_PATH = environmentPath;
			FileServiceLogger.REQUESTS_FOLDER_PATH = FileServiceLogger.ROOT_FOLDER_PATH + "\\request";
			FileServiceLogger.RESULTS_FOLDER_PATH = FileServiceLogger.ROOT_FOLDER_PATH + "\\result";
		}
		this.createFolders();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run()
	{

		try
		{
			final WatchService watcher = FileSystems.getDefault().newWatchService();
			final Path rootFolder = Paths.get(FileServiceLogger.RESULTS_FOLDER_PATH);
			rootFolder.register(watcher, StandardWatchEventKinds.ENTRY_CREATE);

			while (!Thread.currentThread().isInterrupted())
			{
				final WatchKey key = watcher.take();
				for (final WatchEvent<?> event : key.pollEvents())
				{
					// OVERFLOW can happen if we lose an event
					if (event.kind() == StandardWatchEventKinds.OVERFLOW)
					{
						LoggerUtil.getLogger("warn").warn("FileServiceLogger overflow. An event was lost.");
						continue;
					}

					final WatchEvent<Path> eventPath = (WatchEvent<Path>) event;
					final String fileName = eventPath.context().toString();
					final Path filePath = Paths.get(FileServiceLogger.RESULTS_FOLDER_PATH, fileName);
					final File file = filePath.toFile();

					final BufferedReader fileReader = new BufferedReader(new FileReader(file));
					final String firstFileLine = fileReader.readLine();
					if (firstFileLine != null)
					{
						this.parseServiceEvent(firstFileLine);
					}

					fileReader.close();
					file.delete();
				}

				// if the directory is inaccessible
				if (!key.reset())
				{
					LoggerUtil.getLogger("error").error("{} - Current directory is unavailable. Aborting...", this.getClass().getSimpleName());
					break;
				}
			}

		}
		catch (final IOException | InterruptedException e)
		{
			LoggerUtil.getLogger("error").error("{} - {}", this.getClass().getSimpleName(), e.getMessage());
		}
	}

	private void createFolders()
	{
		final File request = new File(FileServiceLogger.REQUESTS_FOLDER_PATH);
		request.mkdirs();
		final File result = new File(FileServiceLogger.RESULTS_FOLDER_PATH);
		result.mkdirs();
	}

	private String getEnvironmentPath()
	{
		return System.getenv().get(FileServiceLogger.ENVIRONMENT_VAR);
	}
}

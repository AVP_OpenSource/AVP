package ca.cyberseclab.avp.core.serviceLogger;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import ca.cyberseclab.avp.util.logger.LoggerUtil;
import ch.qos.logback.classic.Logger;

/**
 * The PortServiceLogger listens on a port for a service event.
 * It acquires an available port at construction. This port is later accessible by calling the getPort method.
 * 
 * @author sfrenette
 */
public class PortServiceLogger extends ServiceLogger
{
	private static PortServiceLogger INSTANCE;

	public static PortServiceLogger getInstance() throws SocketException
	{
		if (PortServiceLogger.INSTANCE == null)
		{
			PortServiceLogger.INSTANCE = new PortServiceLogger();
		}

		return PortServiceLogger.INSTANCE;
	}

	public static boolean isInstanciated()
	{
		return PortServiceLogger.INSTANCE != null;
	}

	private static int PORT_RANGE_BEGIN = 10200;
	private static int PORT_RANGE_END = 12200;

	private final DatagramSocket socket;
	private int port;

	private PortServiceLogger() throws SocketException
	{
		this.socket = this.getSocket(PortServiceLogger.PORT_RANGE_BEGIN, PortServiceLogger.PORT_RANGE_END);
	}

	public int getPort()
	{
		return this.port;
	}

	private DatagramSocket getSocket(final int portRangeBegin, final int portRangeEnd) throws SocketException
	{
		for (int port = portRangeBegin; port <= portRangeEnd; port++)
		{
			final DatagramSocket socket;
			LoggerUtil.getLogger("debug").error("{} - Trying to bind to port {}...", this.getClass().getSimpleName(), port);
			try
			{
				socket = new DatagramSocket(port);
				this.port = port;
				LoggerUtil.getLogger("debug").debug("{} - Successfully bound to port {}.", this.getClass().getSimpleName(), port);
				return socket;
			}
			catch (final SocketException e)
			{
				LoggerUtil.getLogger("debug").debug("{} - Couldn't bind to port {}...", this.getClass().getSimpleName(), port);
			}
		}
		throw new SocketException(String.format("Couldn't bind to any port in range %s to %s.", portRangeBegin, portRangeEnd));
	}

	@Override
	public void run()
	{
		final Logger logger = LoggerUtil.getLogger("debug");
		logger.trace("{} - Starting on port {}...",
						this.getClass().getSimpleName(),
						this.port);
		logger.trace("{} - Waiting for service...", this.getClass().getSimpleName());

		while (!Thread.currentThread().isInterrupted())
		{
			try
			{
				final byte[] buffer = new byte[1024];
				final DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

				this.socket.receive(packet);

				final String serviceText = new String(buffer, 0, packet.getLength());
				this.parseServiceEvent(serviceText);
			}
			catch (final IOException e)
			{
				Thread.currentThread().interrupt();
			}
		}
		this.socket.close();
	}
}

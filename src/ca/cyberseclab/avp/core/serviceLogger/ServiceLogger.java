/**
 * 
 */
package ca.cyberseclab.avp.core.serviceLogger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ca.cyberseclab.avp.core.PhoneBook;
import ca.cyberseclab.avp.core.device.AbstractDevice;
import ca.cyberseclab.avp.util.logger.LoggerUtil;
import ch.qos.logback.classic.Logger;

/**
 * @author sfrenette
 */
public abstract class ServiceLogger extends Thread
{
	public static String SMS_STRING = "sms";
	public static String CALL_STRING = "call";

	public void startLogging()
	{
		if (!this.isStarted())
		{
			this.start();
		}
		else
		{
			LoggerUtil.getLogger("warn").warn("{} is already activated.", this.getClass().getSimpleName());
		}
	}

	public boolean isStarted()
	{
		return this.isAlive();
	}

	@Override
	public abstract void run();

	protected void parseServiceEvent(final String serviceEventString)
	{
		LoggerUtil.getLogger("debug").trace("New service event: " + serviceEventString);
		//         					 sms[555555   SEND   4234234  hello   ]
		final String regex = "^(\\w+?)\\[(.+?)\\s(.+?)\\s(.+?)\\s?(.+?)?\\]$";
		final Pattern pattern = Pattern.compile(regex);
		final Matcher match = pattern.matcher(serviceEventString);

		if (!match.find())
		{
			LoggerUtil.getLogger("error").error("{} - Service event's ({}) syntax is invalid.", this.getClass().getSimpleName(), serviceEventString);
		}

		final String serviceType = match.group(1);
		final String localNumber = match.group(2);
		final AbstractDevice device = PhoneBook.getInstance().getDeviceFromNumber(localNumber);
		final String loggerName = "services." + serviceType + "." + device.getScenarioID();

		final Logger logger = LoggerUtil.getLogger(loggerName, true);
		logger.info(serviceEventString.replace(localNumber, device.getScenarioID()));
	}
}

package ca.cyberseclab.avp.main;

import javafx.application.Application;
import ca.cyberseclab.avp.core.LabManagerMaster;
import ca.cyberseclab.avp.core.ShutdownHook;

/**
 * @author sfrenette
 */
public abstract class AbstractController extends Application
{
	protected Thread labThreadWrapperThread;

	public void close()
	{
		if ((this.labThreadWrapperThread != null) && this.labThreadWrapperThread.isAlive())
		{
			this.labThreadWrapperThread.stop();

			final Thread shutdownHook = new Thread(new ShutdownHook());
			shutdownHook.start();
		}
		this.scenarioExecutionCompleted();
	}

	abstract protected void scenarioExecutionCompleted();

	protected class LabThreadWrapper implements Runnable
	{
		private final LabManagerMaster lab;

		public LabThreadWrapper(final String scenarioPath)
		{
			this.lab = new LabManagerMaster(scenarioPath);
		}

		@Override
		public void run()
		{
			this.lab.run();
			AbstractController.this.scenarioExecutionCompleted();
		}
	}

	public static void main(final String[] args)
	{
		Application.launch(DefaultController.class, args);
	}
}

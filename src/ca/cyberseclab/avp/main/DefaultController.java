/**
 * 
 */
package ca.cyberseclab.avp.main;

import java.io.File;
import java.util.Map;

import javafx.stage.Stage;
import ca.cyberseclab.avp.ui.DefaultUI;

/**
 * @author sfrenette
 */
public class DefaultController extends AbstractController
{
	private DefaultUI ui;

	@Override
	public void start(final Stage primaryStage) throws Exception
	{
		this.ui = new DefaultUI(primaryStage, this);

		final Map<String, String> parameters = this.getParameters().getNamed();
		final String scenarioPath = parameters.get("scenario");
		if ((scenarioPath != null) && !scenarioPath.isEmpty())
		{
			this.startScenario(scenarioPath);
		}
	}

	public void openScenarioRequest()
	{
		final File scenarioResult = this.ui.askUserForScenarioSelection();
		if (scenarioResult != null)
		{
			this.startScenario(scenarioResult.getAbsolutePath());
		}
	}

	private void startScenario(final String scenarioPath)
	{
		this.ui.disableOpenScenario();
		this.labThreadWrapperThread = new Thread(new LabThreadWrapper(scenarioPath));
		this.labThreadWrapperThread.start();
	}

	@Override
	protected void scenarioExecutionCompleted()
	{
		this.ui.enableOpenScenario();
	}
}

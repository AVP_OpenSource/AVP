/**
 * 
 */
package ca.cyberseclab.avp.scenario;

import ca.cyberseclab.avp.core.LabManagerMaster;

/**
 * @author FGagnon
 */
public abstract class AbstractScenarioParser
{
	protected String scenarioFilePath;
	protected LabManagerMaster labMaster;

	public AbstractScenarioParser(final String fileToParse, final LabManagerMaster labMaster)
	{
		this.scenarioFilePath = fileToParse;
		this.labMaster = labMaster;
	}

	public abstract Scenario parse() throws ParserException;
}

/**
 * 
 */
package ca.cyberseclab.avp.scenario;

import java.util.ArrayList;
import java.util.List;

import ca.cyberseclab.avp.core.action.labAction.AbstractLabAction;
import ca.cyberseclab.avp.core.action.labAction.PushFileAction;
import ca.cyberseclab.avp.core.action.labAction.StartAVDAction;
import ca.cyberseclab.avp.core.action.labAction.StopAVDAction;
import ca.cyberseclab.avp.core.action.labAction.WaitForDeviceBootAction;
import ca.cyberseclab.avp.core.device.AbstractDevice;

/**
 * @author sfrenette
 */
public class DummyScenarioParser extends AbstractScenarioParser
{
	public DummyScenarioParser()
	{
		super("", null);
	}

	@Override
	public Scenario parse()
	{
		final Scenario result = new Scenario();

		final Scene resultScene = new Scene();
		result.addScene(resultScene);

		this.labMaster.assignIDToDevice("A", "google", "my_avd1");

		final List<AbstractDevice> tempDeviceList = new ArrayList<AbstractDevice>();
		tempDeviceList.add(this.labMaster.getDeviceFromID("A"));

		final AbstractLabAction startAvd = new StartAVDAction(tempDeviceList, false);
		resultScene.addAction(startAvd);

		final AbstractLabAction waitForBoot = new WaitForDeviceBootAction(tempDeviceList, 300000, 1000);
		resultScene.addAction(waitForBoot);

		final AbstractLabAction pushFile = new PushFileAction(tempDeviceList, "C:\\Dummy2.txt", "/sdcard/");
		resultScene.addAction(pushFile);

		final AbstractLabAction stopAvd = new StopAVDAction(tempDeviceList);
		resultScene.addAction(stopAvd);

		return result;
	}
}

/**
 * 
 */
package ca.cyberseclab.avp.scenario;

/**
 * @author sfrenette
 */
public class ParserException extends Exception
{

	private static final long serialVersionUID = -8954049035125501657L;

	public ParserException()
	{
	}

	public ParserException(final String message)
	{
		super(message);
	}

	public ParserException(final Throwable exception)
	{
		super(exception);
	}
}

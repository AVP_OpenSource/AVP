/**
 * 
 */
package ca.cyberseclab.avp.scenario;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sfrenette
 */
public class Scenario
{
	// package access (pour le validator)
	final Set<Scene> scenes = new HashSet<Scene>();

	// package access
	Scenario()
	{
	}

	// package access
	void addScene(final Scene scene)
	{
		this.scenes.add(scene);
	}

	public Set<Scene> getActiveScenes()
	{
		final Set<Scene> activeScenes = new HashSet<Scene>();
		for (final Scene scene : this.scenes)
		{
			// if scene.precondiditionsCompleted()
			activeScenes.add(scene);
		}
		return activeScenes;
	}
}

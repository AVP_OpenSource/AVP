/**
 * 
 */
package ca.cyberseclab.avp.scenario;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import ca.cyberseclab.avp.core.action.AbstractAction;
import ca.cyberseclab.avp.core.action.exception.ActionExecutionException;


/**
 * @author sfrenette
 */
public class Scene
{
	// package access pour le validator
	final Queue<AbstractAction> actions = new LinkedList<AbstractAction>();
	private Iterator<AbstractAction> iterator;

	public Scene()
	{

	}

	// package access
	void addAction(final AbstractAction action)
	{
		this.actions.add(action);
	}

	public boolean hasMoreActions()
	{
		if (this.iterator == null)
		{
			this.iterator = this.actions.iterator();
		}
		return (this.iterator.hasNext());
	}

	public void executeNextAction() throws ActionExecutionException
	{
		if (this.hasMoreActions())
		{
			final AbstractAction action = this.iterator.next();
			action.execute();
		}
		// TOCHECK what about else!
		// -Exception
		// -return true/false
	}

}

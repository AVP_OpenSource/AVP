/**
 * 
 */
package ca.cyberseclab.avp.scenario;

/**
 * @author sfrenette
 */
public class ValidatorException extends Exception
{
	private static final long serialVersionUID = -6364490585816552082L;

	public ValidatorException()
	{
	}

	public ValidatorException(final String message)
	{
		super(message);
	}

	public ValidatorException(final Throwable exception)
	{
		super(exception);
	}
}

/**
 * 
 */
package ca.cyberseclab.avp.scenario;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ca.cyberseclab.avp.core.LabManagerMaster;
import ca.cyberseclab.avp.core.action.AbstractAction;
import ca.cyberseclab.avp.core.action.deviceAction.CallNumberAction;
import ca.cyberseclab.avp.core.action.deviceAction.NavigateToURLAction;
import ca.cyberseclab.avp.core.action.deviceAction.SendSMSToNumberAction;
import ca.cyberseclab.avp.core.action.labAction.InstallAPPAction;
import ca.cyberseclab.avp.core.action.labAction.LabCallDeviceAction;
import ca.cyberseclab.avp.core.action.labAction.LabSendSMSToDeviceAction;
import ca.cyberseclab.avp.core.action.labAction.PushFileAction;
import ca.cyberseclab.avp.core.action.labAction.ShellCommandAction;
import ca.cyberseclab.avp.core.action.labAction.StartAPPAction;
import ca.cyberseclab.avp.core.action.labAction.StartAVDAction;
import ca.cyberseclab.avp.core.action.labAction.StartMonkeyAction;
import ca.cyberseclab.avp.core.action.labAction.StartRecordCallAction;
import ca.cyberseclab.avp.core.action.labAction.StartRecordNetworkTrafficAction;
import ca.cyberseclab.avp.core.action.labAction.StartRecordProcesses;
import ca.cyberseclab.avp.core.action.labAction.StartRecordSMSAction;
import ca.cyberseclab.avp.core.action.labAction.StartRecordTaintDroid;
import ca.cyberseclab.avp.core.action.labAction.StartStraceAction;
import ca.cyberseclab.avp.core.action.labAction.StopAVDAction;
import ca.cyberseclab.avp.core.action.labAction.StopRecordCallAction;
import ca.cyberseclab.avp.core.action.labAction.StopRecordNetworkTrafficAction;
import ca.cyberseclab.avp.core.action.labAction.StopRecordSMSAction;
import ca.cyberseclab.avp.core.action.labAction.StopStraceAction;
import ca.cyberseclab.avp.core.action.labAction.WaitAction;
import ca.cyberseclab.avp.core.action.labAction.WaitForAppInstallAction;
import ca.cyberseclab.avp.core.action.labAction.WaitForDeviceBootAction;
import ca.cyberseclab.avp.core.device.AbstractDevice;

/**
 * @author sfrenette
 */
public class XMLScenarioParser extends AbstractScenarioParser
{
	private Scenario resultScenario;
	private Scene currentScene;

	public XMLScenarioParser(final String fileToParse, final LabManagerMaster labMaster)
	{
		super(fileToParse, labMaster);
	}

	@Override
	public Scenario parse() throws ParserException
	{
		try
		{
			this.parseScenario(this.getDocument());
		}
		catch (Exception | Error e)
		{
			throw new ParserException(e);
		}
		return this.resultScenario;
	}

	private Document getDocument() throws SAXException, IOException, ParserConfigurationException
	{
		final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		final Schema schema = schemaFactory.newSchema(new File("config/MobileVirtualLabSchema2.xsd"));

		final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		docBuilderFactory.setValidating(false);
		docBuilderFactory.setNamespaceAware(true);
		docBuilderFactory.setSchema(schema);

		DocumentBuilder docBuilder;
		docBuilder = docBuilderFactory.newDocumentBuilder();

		final File file = new File(this.scenarioFilePath);

		final Document doc = docBuilder.parse(file);
		doc.getDocumentElement().normalize();

		return doc;
	}

	private void parseScenario(final Document doc) throws ParserException
	{
		this.resultScenario = new Scenario();

		// parse every node
		final NodeList nodeList = doc.getElementsByTagName("*");
		for (int i = 0; i < nodeList.getLength(); i++)
		{
			this.parseScenarioNode(nodeList.item(i));
		}
	}

	private void parseScenarioNode(final Node node) throws ParserException
	{
		if (node instanceof Element)
		{
			if (node.getNodeName().equals("device"))
			{
				this.parseDevice((Element) node);
			}
			else if (node.getNodeName().equals("scene"))
			{
				this.parseScene((Element) node);
			}
		}
	}

	private void parseDevice(final Element device)
	{
		final NamedNodeMap attributes = device.getAttributes();
		final String id = attributes.getNamedItem("id").getNodeValue();
		final String technology = attributes.getNamedItem("technology").getNodeValue();
		final String name = attributes.getNamedItem("name").getNodeValue();

		this.labMaster.assignIDToDevice(id, technology, name);

		this.parseTechSpecificDeviceAttribs(technology, attributes);
	}

	//<lafrancef>
	private void parseTechSpecificDeviceAttribs(final String tech, final NamedNodeMap attributes)
	{
		final String deviceName = attributes.getNamedItem("name").getNodeValue();
		if (AbstractDevice.TECH_PHYSICAL.equals(tech))
		{
			//Name and System Id are equivalent for physical devices
			this.labMaster.assignSystemIdToDevice(deviceName, tech, deviceName);
		}
		else if (AbstractDevice.TECH_GENYMOTION.equals(tech))
		{
			//An additional attribute gives the system id for geny devices
			final String ip = attributes.getNamedItem("ip").getNodeValue();
			this.labMaster.assignSystemIdToDevice(ip + ":5555", tech, deviceName);
		}
	}

	//</lafrancef>

	private void parseScene(final Element sceneElement) throws ParserException
	{
		final Scene newScene = new Scene();
		this.currentScene = newScene;
		this.resultScenario.addScene(newScene);

		final NodeList nodeList = sceneElement.getElementsByTagName("*");
		for (int i = 0; i < nodeList.getLength(); i++)
		{
			this.parseSceneNode(nodeList.item(i));
		}

		this.resultScenario.addScene(newScene);
	}

	private void parseSceneNode(final Node node) throws ParserException
	{
		if (node instanceof Element)
		{
			if (node.getNodeName().equals("preconditions"))
			{
				this.parseScenePreconditions((Element) node);
			}
			else if (node.getNodeName().equals("actions"))
			{
				this.parseSceneActions((Element) node);
			}
		}
	}

	private void parseScenePreconditions(final Element preconditions)
	{
		// TODO loop through preconditions
	}

	/*private void parseScenePreconditionsNode(final Node node)
	{
		// TODO parse precondition
	}*/

	private void parseSceneActions(final Element actions) throws ParserException
	{
		final NodeList nodeList = actions.getElementsByTagName("*");
		for (int i = 0; i < nodeList.getLength(); i++)
		{
			this.parseSceneActionsNode(nodeList.item(i));
		}
	}

	private void parseSceneActionsNode(final Node node) throws ParserException
	{
		if (node instanceof Element)
		{
			final Element action = (Element) node;
			final String actionType = action.getNodeName();
			switch (actionType)
			{
			case "LAB_StartAVD":
				this.parseActionStartAVD(action);
				break;
			case "LAB_StopAVD":
				this.parseActionStopAVD(action);
				break;
			case "LAB_StartRecordProcesses":
				this.parseActionStartRecordProcesses(action);
				break;
			case "LAB_StartRecordNetworkTraffic":
				this.parseActionStartRecordNetworkTraffic(action);
				break;
			case "LAB_StopRecordNetworkTraffic":
				this.parseActionStopNetworkTraffic(action);
				break;
			case "LAB_StartRecordSMS":
				this.parseActionStartRecordSMS(action);
				break;
			case "LAB_StopRecordSMS":
				this.parseActionStopRecordSMS(action);
				break;
			case "LAB_StartStrace":
				this.parseActionStartStrace(action);
				break;
			case "LAB_StopStrace":
				this.parseActionStopStrace(action);
				break;
			case "LAB_SendSMSToDevice":
				this.parseActionLabSendSMSToDevice(action);
				break;
			case "DEVICE_SendSMSToNumber":
				this.parseActionSendSMSToNumber(action);
				break;
			case "DEVICE_SendSMSToDevice":
				this.parseActionSendSMSToNumber(action);
				break;
			case "LAB_StartRecordCall":
				this.parseActionStartRecordCall(action);
				break;
			case "LAB_StopRecordCall":
				this.parseActionStopRecordCall(action);
				break;
			case "LAB_StartRecordTaintDroid":
				this.parseActionStartRecordTaintDroid(action);
				break;
			case "LAB_StopRecordTaintDroid":
				this.parseActionStopRecordTaintDroid(action);
				break;
			case "LAB_CallDevice":
				this.parseActionLabCallDevice(action);
				break;
			case "DEVICE_CallNumber":
				this.parseActionCallNumber(action);
				break;
			case "DEVICE_CallDevice":
				this.parseActionCallNumber(action);
				break;
			case "DEVICE_NavigateToURL":
				this.parseActionNavigateToURL(action);
				break;
			case "LAB_PushFile":
				this.parseActionPushFile(action);
				break;
			case "LAB_PullFile":
				this.parseActionPullFile(action);
				break;
			case "LAB_InstallAPP":
				this.parseActionInstallAPP(action);
				break;
			case "LAB_WaitForAppInstall":
				this.parseActionWaitForAppInstall(action);
				break;
			case "LAB_StartAPP":
				this.parseActionStartAPP(action);
				break;
			case "LAB_ShellCommand":
				this.parseActionShellCommand(action);
				break;
			case "LAB_WaitForDeviceBoot":
				this.parseActionWaitForDeviceBoot(action);
				break;
			case "LAB_StartMonkey":
				this.parseActionStartMonkey(action);
				break;
			case "LAB_Wait":
				this.parseActionWait(action);
				break;
			default:
				break;
			}
		}
	}

	/**
	 * Parse a devices string and returns the appropriate list of devices.
	 * 
	 * @param devicesString
	 * @return the appropriate list of devices
	 * @throws ParserException
	 */
	private List<AbstractDevice> parseDevicesString(final String devicesString) throws ParserException
	{
		final List<AbstractDevice> deviceList = new ArrayList<AbstractDevice>();
		for (final String deviceCustomID : devicesString.split(" "))
		{
			try
			{
				deviceList.add(this.labMaster.getDeviceFromID(deviceCustomID));
			}
			catch (final NoSuchElementException e)
			{
				throw new ParserException(e.getMessage());
			}
		}
		return deviceList;
	}

	/**
	 * Converts a time amount with the specified type in ms.
	 * Valid type formats are: (s, m, h).
	 * Invalid type formats return the same amount.
	 * 
	 * @param type
	 * @param amount
	 * @return the time amount converted in millisecond
	 */
	private long parseTimeString(final String type, final String amount)
	{
		if (type.equals("s"))
		{
			long amountInMS = Long.parseLong(amount);
			amountInMS *= 1000;
			return amountInMS;
		}
		else if (type.equals("m"))
		{
			long amountInMS = Long.parseLong(amount);
			amountInMS *= 60000;
			return amountInMS;
		}
		else if (type.equals("h"))
		{
			long amountInMS = Long.parseLong(amount);
			amountInMS *= 3600000;
			return amountInMS;
		}
		else
		{
			return Long.parseLong(amount);
		}
	}

	private void parseActionStartAVD(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");

		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);
		final boolean persistent = Boolean.parseBoolean(actionElement.getAttribute("persistent"));

		final AbstractAction resultAction = new StartAVDAction(devices, persistent);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionStopAVD(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");

		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final AbstractAction resultAction = new StopAVDAction(devices);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionStartRecordProcesses(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");
		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final AbstractAction resultAction = new StartRecordProcesses(devices);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionStartRecordNetworkTraffic(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");
		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final String interfaceString = actionElement.getAttribute("interface");

		final AbstractAction resultAction = new StartRecordNetworkTrafficAction(devices, interfaceString);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionStopNetworkTraffic(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");
		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final AbstractAction resultAction = new StopRecordNetworkTrafficAction(devices);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionStartRecordSMS(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");
		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final AbstractAction resultAction = new StartRecordSMSAction(devices);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionStopRecordSMS(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");
		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final AbstractAction resultAction = new StopRecordSMSAction(devices);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionSendSMSToNumber(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");

		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);
		final String message = actionElement.getAttribute("message");
		final String toNumber = actionElement.getAttribute("to-number");

		final AbstractAction resultAction = new SendSMSToNumberAction(devices, message, toNumber);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionStartRecordCall(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");
		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final AbstractAction resultAction = new StartRecordCallAction(devices);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionStopRecordCall(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");
		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final AbstractAction resultAction = new StopRecordCallAction(devices);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionStartRecordTaintDroid(final Element action) throws ParserException
	{
		final String devicesString = action.getAttribute("devices");
		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final AbstractAction resultAction = new StartRecordTaintDroid(devices);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionStopRecordTaintDroid(final Element action) throws ParserException
	{
		final String devicesString = action.getAttribute("devices");
		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final AbstractAction resultAction = new StartRecordTaintDroid(devices);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionLabCallDevice(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");
		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final String fromNumber = actionElement.getAttribute("from-number");

		final AbstractAction resultAction = new LabCallDeviceAction(devices, fromNumber);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionLabSendSMSToDevice(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");
		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final String message = actionElement.getAttribute("message");
		final String fromNumber = actionElement.getAttribute("from-number");

		final AbstractAction resultAction = new LabSendSMSToDeviceAction(devices, message, fromNumber);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionCallNumber(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");

		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);
		final String toNumber = actionElement.getAttribute("to-number");

		final AbstractAction resultAction = new CallNumberAction(devices, toNumber);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionNavigateToURL(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");

		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);
		final String url = actionElement.getAttribute("url");

		final AbstractAction resultAction = new NavigateToURLAction(devices, url);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionPushFile(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");

		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final String localPath = actionElement.getAttribute("localPath");
		final String remotePath = actionElement.getAttribute("remotePath");

		final AbstractAction resultAction = new PushFileAction(devices, localPath, remotePath);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionPullFile(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");

		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final String remotePath = actionElement.getAttribute("remotePath");
		final String localPath = actionElement.getAttribute("localPath");

		final AbstractAction resultAction = new PushFileAction(devices, remotePath, localPath);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionInstallAPP(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");

		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final String localPath = actionElement.getAttribute("localPathToAPK");
		final String reinstallString = actionElement.getAttribute("reinstall");
		final boolean reinstall = Boolean.parseBoolean(reinstallString);

		final AbstractAction resultAction = new InstallAPPAction(devices, localPath, reinstall);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionWaitForAppInstall(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");

		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final String localPath = actionElement.getAttribute("localPathToAPK");

		final AbstractAction resultAction = new WaitForAppInstallAction(devices, localPath);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionStartAPP(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");

		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final String localPath = actionElement.getAttribute("localPathToAPK");

		final AbstractAction resultAction = new StartAPPAction(devices, localPath);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionShellCommand(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");

		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final String command = actionElement.getAttribute("command");

		final AbstractAction resultAction = new ShellCommandAction(devices, command);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionWaitForDeviceBoot(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");

		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final String timeType = actionElement.getAttribute("timeType");
		final long timeout = this.parseTimeString(timeType, actionElement.getAttribute("timeout"));
		final long interval = this.parseTimeString(timeType, actionElement.getAttribute("interval"));

		final AbstractAction resultAction = new WaitForDeviceBootAction(devices, timeout, interval);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionStartMonkey(final Element actionElement) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");
		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final int numberOfEvents = Integer.parseInt(actionElement.getAttribute("numberOfEvents"));
		final String packageName = actionElement.getAttribute("package");
		final String apkName = actionElement.getAttribute("localPathToAPK");

		final AbstractAction resultAction = new StartMonkeyAction(devices, numberOfEvents, packageName, apkName);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionStartStrace(final Element actionElement) throws ParserException
	{
		this.parseActionStrace(actionElement, true);
	}

	private void parseActionStopStrace(final Element actionElement) throws ParserException
	{
		this.parseActionStrace(actionElement, false);
	}

	private void parseActionStrace(final Element actionElement, final boolean start) throws ParserException
	{
		final String devicesString = actionElement.getAttribute("devices");
		final List<AbstractDevice> devices = this.parseDevicesString(devicesString);

		final String processName = actionElement.getAttribute("process");

		final AbstractAction resultAction = start ? new StartStraceAction(devices, processName) : new StopStraceAction(devices, processName);
		this.currentScene.addAction(resultAction);
	}

	private void parseActionWait(final Element actionElement) throws ParserException
	{
		final String timeType = actionElement.getAttribute("timeType");
		final long amount = this.parseTimeString(timeType, actionElement.getAttribute("amount"));

		final AbstractAction resultAction = new WaitAction(amount);
		this.currentScene.addAction(resultAction);
	}
}
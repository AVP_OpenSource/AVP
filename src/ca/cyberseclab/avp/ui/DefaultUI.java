/**
 * 
 */
package ca.cyberseclab.avp.ui;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import ca.cyberseclab.avp.main.DefaultController;

/**
 * @author sfrenette
 */
public class DefaultUI
{
	private final DefaultController controller;
	private final Stage stage;

	private final VBox container = new VBox();

	private final MenuBar menuBar = new MenuBar();

	final Menu menuFile = new Menu("File");
	final MenuItem menuItemOpenScenario = new MenuItem("Open Scenario...");

	final Menu menuAction = new Menu("Action");
	final MenuItem menuItemStop = new MenuItem("Stop");

	public DefaultUI(final Stage primaryStage, final DefaultController controller)
	{
		this.controller = controller;
		this.stage = primaryStage;

		this.menuItemOpenScenario.setOnAction(this.onOpenScenarioAction);
		this.menuFile.getItems().add(this.menuItemOpenScenario);

		this.menuItemStop.setOnAction(this.onStopAction);
		this.menuAction.getItems().add(this.menuItemStop);

		this.menuBar.getMenus().addAll(this.menuFile, this.menuAction);

		HBox.setHgrow(this.menuBar, Priority.ALWAYS);

		this.container.getChildren().add(this.menuBar);

		primaryStage.setTitle("Default Viewer");
		primaryStage.setScene(new Scene(this.container, 200, 100));
		primaryStage.setResizable(false);
		primaryStage.show();

		primaryStage.setOnCloseRequest(this.onClose);
	}

	public File askUserForScenarioSelection()
	{
		final FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Scenario", "*.xml"));
		final File scenario = fileChooser.showOpenDialog(DefaultUI.this.stage);

		return scenario;
	}

	public void disableOpenScenario()
	{
		this.menuItemOpenScenario.setDisable(true);
	}

	public void enableOpenScenario()
	{
		this.menuItemOpenScenario.setDisable(false);
	}

	private final EventHandler<ActionEvent> onOpenScenarioAction = new EventHandler<ActionEvent>()
	{
		@Override
		public void handle(final ActionEvent event)
		{
			DefaultUI.this.controller.openScenarioRequest();
		}
	};

	private final EventHandler<ActionEvent> onStopAction = new EventHandler<ActionEvent>()
	{
		@Override
		public void handle(final ActionEvent event)
		{
			DefaultUI.this.controller.close();
		}
	};

	private final EventHandler<WindowEvent> onClose = new EventHandler<WindowEvent>()
	{
		@Override
		public void handle(final WindowEvent event)
		{
			DefaultUI.this.controller.close();
		}
	};

}

/**
 * 
 */
package ca.cyberseclab.avp.util;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ca.cyberseclab.avp.util.command.Command;
import ca.cyberseclab.avp.util.command.CommandLineWrapper;
import ca.cyberseclab.avp.util.setting.Settings;

/**
 * @author sfrenette
 */
public class AndroidManifest
{
	private static final String PATTERN_PACKAGE = "package=\"(.+?)\"";
	private static final String PATTERN_MAIN_ACTIVITY = "(?s)activity(?:.*?)=\"(.+?)\"(?:.*)MAIN\"";
	private static final String PATTERN_PERMISSIONS = "(?s)uses-permission(?:.*?)=\"(.+?)\"";
	private static final String PATTERN_ACTIVITIES = "(?s)activity(?:.*?)=\"(.+?)\"";
	private static final String PATTERN_INTENTS = "(?s)E:\\s(?:action|category)(?:.*?)=\"(.+?)\"";

	private final String localPathToAPK;
	private String manifest;

	private String packageName = "";
	private String mainActivity = "";
	private Set<String> permissions = Collections.emptySet();
	private Set<String> activities = Collections.emptySet();
	private final Set<String> intents = Collections.emptySet();

	public AndroidManifest(final String localPathToAPK) throws IOException
	{
		this.localPathToAPK = localPathToAPK;
		this.load();
	}

	private void load() throws IOException
	{
		final Command command = new Command(Settings.get(Settings.AAPT_PATH_KEY))
						.addParameter("l")
						.addParameter(true, "-a", this.localPathToAPK);

		this.manifest = CommandLineWrapper.executeBlockingCommand(command);
		// the manifest starts at this index in the command result
		final int manifestIndex = this.manifest.indexOf("N:");
		this.manifest = this.manifest.substring(manifestIndex);
	}

	public String getPackageName() throws NoSuchElementException
	{
		if (this.packageName.isEmpty())
		{
			final Pattern pattern = Pattern.compile(AndroidManifest.PATTERN_PACKAGE);
			final Matcher matcher = pattern.matcher(this.manifest);
			if (matcher.find())
			{
				this.packageName = matcher.group(1);
				return this.packageName;
			}
			throw new NoSuchElementException();
		}
		return this.packageName;
	}

	public String getMainActivityName() throws NoSuchElementException
	{
		if (this.mainActivity.isEmpty())
		{
			final Pattern pattern = Pattern.compile(AndroidManifest.PATTERN_MAIN_ACTIVITY);
			final Matcher matcher = pattern.matcher(this.manifest);

			if (matcher.find())
			{
				String result = matcher.group(1);
				result = result.substring(result.lastIndexOf(".") + 1);
				this.mainActivity = result;
				return this.mainActivity;
			}
			throw new NoSuchElementException();
		}

		return this.mainActivity;
	}

	public Set<String> getPermissions()
	{
		if (this.permissions.isEmpty())
		{
			final Pattern pattern = Pattern.compile(AndroidManifest.PATTERN_PERMISSIONS);
			final Matcher matcher = pattern.matcher(this.manifest);
			final Set<String> permissions = new HashSet<String>();
			while (matcher.find())
			{
				permissions.add(matcher.group(1));
			}
			this.permissions = permissions;
		}
		return this.permissions;
	}

	public Set<String> getActivities() throws NoSuchElementException
	{
		if (this.activities.isEmpty())
		{
			final Pattern pattern = Pattern.compile(AndroidManifest.PATTERN_ACTIVITIES);
			final Matcher matcher = pattern.matcher(this.manifest);
			final Set<String> activities = new HashSet<String>();
			while (matcher.find())
			{
				activities.add(matcher.group(1));
			}
			this.activities = activities;
		}
		return this.activities;
	}

	public Set<String> getIntents() throws NoSuchElementException
	{
		if (this.intents.isEmpty())
		{
			final Pattern pattern = Pattern.compile(AndroidManifest.PATTERN_INTENTS);
			final Matcher matcher = pattern.matcher(this.manifest);
			final Set<String> activities = new HashSet<String>();
			while (matcher.find())
			{
				activities.add(matcher.group(1));
			}
			this.activities = activities;
		}
		return this.activities;
	}

}

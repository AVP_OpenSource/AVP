/**
 * 
 */
package ca.cyberseclab.avp.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author sfrenette
 */
public class DateStringUtil
{
	public static String get()
	{
		return DateStringUtil.get("yyyy'_'MM'_'dd_H'h'mm'm'ss's'");
	}

	public static String get(final String format)
	{
		final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(new Date());
	}

	private DateStringUtil()
	{
	}

}

/**
 * 
 */
package ca.cyberseclab.avp.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * @author sfrenette
 */
public class PsParser
{
	// Map<Process, PID>
	private final Map<String, String> psProcesses = new HashMap<String, String>();

	public PsParser(final String psResult) throws IOException
	{
		final BufferedReader bufferedReader = new BufferedReader(new StringReader(psResult));

		// the first line is the psResult columns header, so we skip it
		bufferedReader.readLine();

		for (String currentLine; (currentLine = bufferedReader.readLine()) != null;)
		{
			final String[] processColumns = currentLine.split("\\s+");
			// the first column is PID
			final String PID = processColumns[1];
			// the eight column is NAME
			final String processName = processColumns[8];
			this.psProcesses.put(processName, PID);
		}
		bufferedReader.close();
	}

	public String getPIDFromProcessName(final String processName)
	{
		final String PID = this.psProcesses.get(processName);
		if (PID == null)
		{
			throw new NoSuchElementException();
		}
		return PID;
	}

	public boolean containsProcess(final String processName)
	{
		return this.psProcesses.containsKey(processName);
	}
}

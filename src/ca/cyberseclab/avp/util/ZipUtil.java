package ca.cyberseclab.avp.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FilenameUtils;

/**
 * @author sfrenette
 */
public class ZipUtil
{
	public static void unzip(final File fileToUnzip) throws IOException
	{
		final String parentFolder = fileToUnzip.getParent();
		final String fileName = FilenameUtils.getBaseName(fileToUnzip.getAbsolutePath());
		final File folderToUnzipTo = Paths.get(parentFolder, fileName).toFile();

		ZipUtil.unzip(fileToUnzip, folderToUnzipTo);
	}

	public static void unzip(final File fileToUnzip, final File folderToUnzipTo) throws IOException
	{
		final ZipInputStream zipInput = new ZipInputStream(new FileInputStream(fileToUnzip));
		ZipEntry zipEntry;
		while ((zipEntry = zipInput.getNextEntry()) != null)
		{
			// creating file
			final String entryName = zipEntry.getName();
			final File entryFile = Paths.get(folderToUnzipTo.getAbsolutePath(), entryName).toFile();
			entryFile.getParentFile().mkdirs();

			// writing to file
			final FileOutputStream fileStream = new FileOutputStream(entryFile);
			int writeLength;
			final byte buffer[] = new byte[1024];
			while ((writeLength = zipInput.read(buffer)) > 0)
			{
				fileStream.write(buffer, 0, writeLength);
			}

			fileStream.close();
		}
		zipInput.closeEntry();
		zipInput.close();
	}

	public static void zipFolder(final File folderToZip, final File resultZip) throws IOException
	{
		final ZipOutputStream zipOutput = new ZipOutputStream(new FileOutputStream(resultZip));
		ZipUtil.addFilesToZipRecursively(zipOutput, folderToZip, folderToZip);
		zipOutput.close();
	}

	private static void addFilesToZipRecursively(final ZipOutputStream zipOutput, final File sourceFolder, final File currentFile) throws IOException
	{
		if (currentFile.isDirectory())
		{
			final File childFiles[] = currentFile.listFiles();
			for (final File currentChildFile : childFiles)
			{
				ZipUtil.addFilesToZipRecursively(zipOutput, sourceFolder, currentChildFile);
			}
		}
		else
		{
			// creating file
			final String entryName = ZipUtil.getFileRelativePath(currentFile, sourceFolder);
			final ZipEntry zipEntry = new ZipEntry(entryName);
			zipOutput.putNextEntry(zipEntry);

			// writing to file
			final FileInputStream fileStream = new FileInputStream(currentFile);
			int readLength;
			final byte[] buffer = new byte[1024];
			while ((readLength = fileStream.read(buffer)) > 0)
			{
				zipOutput.write(buffer, 0, readLength);
			}

			fileStream.close();
			zipOutput.closeEntry();
		}
	}

	private static String getFileRelativePath(final File file, final File relativeFromFolder)
	{
		final String relativeFromFolderPath = relativeFromFolder.getAbsolutePath();
		final String filePath = file.getAbsolutePath();

		// removes the base folder path from the specified file path
		// ex: C:/test/test2/test3.txt and C:/test gives test2/test3.txt 
		final String result = filePath.substring(relativeFromFolderPath.length() + 1, filePath.length());

		return result;
	}

	private ZipUtil()
	{

	}
}

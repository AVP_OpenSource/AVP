package ca.cyberseclab.avp.util.command;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import ch.qos.logback.classic.Logger;

public class Command
{
	private final List<Entry<String, String[]>> parameters = new ArrayList<Entry<String, String[]>>();
	private final String executable;

	Map<String, String> environmentVariables = new HashMap<String, String>();
	boolean hasEnvironmentVariables = false;
	Logger logger;
	boolean hasLogger = false;

	public Command(final String executable)
	{
		this.executable = executable;
	}

	public Command setEnvironmentVariable(final String variableName, final String variableValue)
	{
		this.environmentVariables.put(variableName, variableValue);
		this.hasEnvironmentVariables = true;
		return this;
	}

	public Command setLogger(final Logger logger)
	{
		this.logger = logger;
		this.hasLogger = true;
		return this;
	}

	public Command addParameter(final String option, final String... optionParams)
	{
		return this.addParameter(false, option, optionParams);
	}

	public Command addParameter(final boolean addQuotes, final String option, final String... optionParams)
	{
		final Entry<String, String[]> entry;
		if (addQuotes)
		{
			final String[] paramsWithDoubleQuotes = this.addDoubleQuotesToParams(optionParams);
			entry = new SimpleEntry<String, String[]>(option, paramsWithDoubleQuotes);
		}
		else
		{
			entry = new SimpleEntry<String, String[]>(option, optionParams);
		}

		this.parameters.add(entry);
		return this;
	}

	private String[] addDoubleQuotesToParams(final String[] params)
	{
		final String[] result = new String[params.length];
		for (int i = 0; i < params.length; i++)
		{
			if (!params[i].startsWith("\"") && !params[i].endsWith("\""))
			{
				result[i] = "\"" + params[i] + "\"";
			}
		}
		return result;
	}

	public String getExecutable()
	{
		return this.executable;
	}

	String parametersToString()
	{
		final StringBuilder result = new StringBuilder();
		// adding parameters and their options to string
		for (final Entry<String, String[]> entry : this.parameters)
		{
			result.append(entry.getKey());
			result.append(" ");
			// adding every options
			for (final String optionParam : entry.getValue())
			{
				result.append(optionParam);
				result.append(" ");
			}
		}
		if (this.parameters.size() > 0)
		{
			// removing the last whitespace
			result.setLength(result.length() - 1);
		}
		return result.toString();
	}

	@Override
	public String toString()
	{
		final StringBuilder result = new StringBuilder();
		// adding executable to string
		result.append(this.executable);
		result.append(" ");

		// adding parameters and their options
		result.append(this.parametersToString());

		return result.toString();
	}

	// package access
	List<String> toStringList()
	{
		final List<String> result = new ArrayList<String>();
		result.add(this.executable);
		for (final Entry<String, String[]> entry : this.parameters)
		{
			result.add(entry.getKey());
			for (final String optionParam : entry.getValue())
			{
				result.add(optionParam);
			}
		}
		return result;
	}
}

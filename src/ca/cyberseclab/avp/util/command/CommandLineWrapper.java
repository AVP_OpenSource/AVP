/**
 * 
 */
package ca.cyberseclab.avp.util.command;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.telnet.TelnetClient;

import ca.cyberseclab.avp.util.logger.LoggerUtil;
import ch.qos.logback.classic.Logger;

/**
 * @author sfrenette
 */
public class CommandLineWrapper
{
	/**
	 * Executes a non-blocking command. The method will not wait for the command to finish before returning
	 * 
	 * @param command The command to execute
	 * @return An object to manage the command's execution
	 * @throws IOException
	 */
	public static CommandResult executeNonblockingCommand(final Command command) throws IOException
	{
		final ProcessBuilder processBuilder = new ProcessBuilder(command.toStringList());
		final Logger logger;

		// ENVIRONMENT
		if (command.hasEnvironmentVariables)
		{
			CommandLineWrapper.addEnvironmentVariablesToProcessBuilder(processBuilder, command.environmentVariables);
		}

		// LOGGER
		if (command.hasLogger)
		{
			logger = command.logger;
		}
		else
		{
			// the logger name is the command's executable's name (EX: adb)
			final String loggerName = "processes." + FilenameUtils.getBaseName(new File(command.getExecutable()).getName());
			logger = LoggerUtil.getLogger(loggerName);
		}

		logger.info("Executing command: {}...", command.toString());

		// executing command
		return new CommandResult(processBuilder.start(), logger);
	}

	/**
	 * Execute a blocking command. The method will wait until the command is completed before returning.
	 * 
	 * @param command The command to execute
	 * @return The output of the command
	 * @throws IOException
	 */
	public static String executeBlockingCommand(final Command command) throws IOException
	{
		final CommandResult ongoingCommand = CommandLineWrapper.executeNonblockingCommand(command);
		try
		{
			ongoingCommand.waitForCompletion();
		}
		catch (final InterruptedException e)
		{
			//This should not happen, so do nothing for now
		}
		return ongoingCommand.getOutput(CommandResult.Stream.STREAM_BOTH);
	}

	private static void addEnvironmentVariablesToProcessBuilder(final ProcessBuilder processBuilder, final Map<String, String> environmentVars)
	{
		final Map<String, String> currentEnvironment = processBuilder.environment();
		currentEnvironment.putAll(environmentVars);
	}

	public static void executeTelnetCommand(final String hostname, final String port, final Command... commands) throws IOException
	{
		final TelnetClient telnetClient = new TelnetClient();

		try
		{
			telnetClient.connect(hostname, Integer.parseInt(port));
			final PrintStream telnetOutputStream = new PrintStream(telnetClient.getOutputStream());

			// executing every command
			for (final Command command : commands)
			{
				telnetOutputStream.println(command.toString());
			}

			telnetOutputStream.flush();
			// we have to read from the stream: if we don't our commands won't get executed
			telnetClient.getInputStream().read();
			telnetClient.disconnect();
		}
		catch (NumberFormatException | IOException e)
		{
			final String errorMessage = String.format("%s - Couldn't connect to telnet with %s and port %s." +
							"\nError description: %s", CommandLineWrapper.class.getSimpleName(), hostname, port, e.getMessage());
			LoggerUtil.getLogger("warn").warn(errorMessage);
			throw e;
		}

	}
}

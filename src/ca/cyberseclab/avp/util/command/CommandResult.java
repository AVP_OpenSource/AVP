/**
 * 
 */
package ca.cyberseclab.avp.util.command;

import java.io.PrintStream;

import ch.qos.logback.classic.Logger;

/**
 * A representation of the result of a command execution.
 * 
 * @author lafrancef
 */
public class CommandResult
{
	public enum Stream
	{
		STREAM_STANDARD, STREAM_ERROR, STREAM_BOTH
	};

	private final Process spawnedProcess;
	private final Thread stdoutCapture;
	private final Thread stderrCapture;

	private final PrintStream stdin;

	//StringBuffer is used here because it is threadsafe
	private final StringBuffer stdout = new StringBuffer();
	private final StringBuffer stderr = new StringBuffer();
	private final StringBuffer allOutputs = new StringBuffer(); //Both stdout and stderr, interleaved

	/**
	 * Creates a CommandResult managing the given process
	 * 
	 * @param source
	 * @param commandLogger
	 */
	public CommandResult(final Process source, final Logger commandLogger)
	{
		this.spawnedProcess = source;

		this.stdoutCapture = new Thread(new ProcessStreamCapture(source.getInputStream(), commandLogger,
						this.stdout, this.allOutputs), "ProcessCapture/" + commandLogger.getName() + "/stdout");
		this.stderrCapture = new Thread(new ProcessStreamCapture(source.getErrorStream(), commandLogger,
						this.stderr, this.allOutputs), "ProcessCapture/" + commandLogger.getName() + "/stderr");

		this.stdoutCapture.start();
		this.stderrCapture.start();

		this.stdin = new PrintStream(source.getOutputStream());
	}

	/**
	 * @return True if the process spawned by this command is still running, false otherwise
	 */
	public boolean isRunning()
	{
		try
		{
			//An IllegalThreadStateException is thrown by this method if the thread is not terminated
			this.spawnedProcess.exitValue();
			return false;
		}
		catch (final IllegalThreadStateException e)
		{
			return true;
		}
	}

	public void waitForCompletion() throws InterruptedException
	{
		this.spawnedProcess.waitFor();
		this.stdoutCapture.join();
		this.stderrCapture.join();
	}

	public void stop()
	{
		//This will also close the process' output streams, stopping the capture threads
		this.spawnedProcess.destroy();
	}

	/**
	 * Gets the output of one of the underlying process' streams.
	 * 
	 * @throws IllegalStateException If the process launched by this command is still running
	 */
	public String getOutput(final CommandResult.Stream which) throws IllegalStateException
	{
		if (this.isRunning())
		{
			throw new IllegalStateException("Cannot get the result of an on-going command");
		}

		switch (which)
		{
		case STREAM_STANDARD:
			return this.stdout.toString();
		case STREAM_ERROR:
			return this.stderr.toString();
		case STREAM_BOTH:
			return this.allOutputs.toString();
		default:
			return null;
		}
	}

	public void writeToStdin(final String what)
	{
		this.stdin.println(what);
	}
}

package ca.cyberseclab.avp.util.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import ch.qos.logback.classic.Logger;

/**
 * @author sfrenette
 */
public class ProcessStreamCapture implements Runnable
{
	private final InputStream processOutput;
	private final Logger logger;
	private final StringBuffer[] captureBuffers;

	/**
	 * Creates an object that will continuously capture the output of a process
	 * 
	 * @param processOutput An output of a process
	 * @param logger A logger in which the process output will be written
	 * @param captureBuffers One or several buffers in which the process output should be written
	 */
	public ProcessStreamCapture(final InputStream processOutput, final Logger logger, final StringBuffer... captureBuffers)
	{
		this.processOutput = processOutput;
		this.logger = logger;
		this.captureBuffers = captureBuffers;
	}

	@Override
	public void run()
	{
		try
		{
			final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.processOutput));
			String currentLine;
			while ((currentLine = bufferedReader.readLine()) != null)
			{
				currentLine = currentLine.replaceAll("\\r|\\n", "");
				if (!currentLine.isEmpty())
				{
					this.newLine(currentLine);
				}
			}
		}
		catch (final IOException e)
		{
			// continue lab
		}
	}

	private void newLine(final String line)
	{
		for (int i = 0; i < this.captureBuffers.length; i++)
		{
			this.captureBuffers[i].append(line);
			this.captureBuffers[i].append(System.getProperty("line.separator"));
		}
		this.logger.info(line);
	}
}

package ca.cyberseclab.avp.util.logger;

import ch.qos.logback.classic.spi.ILoggingEvent;

public interface LogViewer
{
	public void addLog(final ILoggingEvent log, final String loggerName);

	public void addLog(final String log, final String loggerName);
}

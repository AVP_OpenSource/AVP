package ca.cyberseclab.avp.util.logger;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;

/**
 * @author sfrenette
 */
public class LogViewerAppender extends AppenderBase<ILoggingEvent>
{
	private final LogViewer logViewer;
	private final String loggerName;

	public LogViewerAppender(final LogViewer logViewer, final String loggerName)
	{
		this.logViewer = logViewer;
		this.loggerName = loggerName;
	}

	@Override
	protected void append(final ILoggingEvent event)
	{
		this.logViewer.addLog(event, this.loggerName);
	}
}

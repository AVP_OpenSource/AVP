package ca.cyberseclab.avp.util.logger;

import org.slf4j.LoggerFactory;

import ca.cyberseclab.avp.core.LabManagerMaster;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;

/**
 * @author sfrenette
 */
public class LoggerUtil
{
	private static LogViewer LOG_VIEWER = null;

	private static String FILE_EXTENSION = "txt";
	private static String FILE_APPENDER_PREFIX = "file_";
	private static String GUI_APPENDER_PREFIX = "gui_";

	public static void setLogViewer(final LogViewer logViewer)
	{
		LoggerUtil.LOG_VIEWER = logViewer;
	}

	public static Logger getRootLogger()
	{
		return LoggerUtil.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME, false);
	}

	public static Logger getLogger(final String loggerName)
	{
		return LoggerUtil.getLogger(loggerName, false);
	}

	public static Logger getLogger(@SuppressWarnings("rawtypes") final Class clazz, final boolean appendToParent)
	{
		return LoggerUtil.getLogger(clazz.getSimpleName(), appendToParent);
	}

	public static Logger getLogger(final String loggerName, final boolean appendToParent)
	{
		final Logger logger = (Logger) LoggerFactory.getLogger(loggerName);
		logger.setLevel(Level.ALL);
		LoggerUtil.addAppenders(logger);

		if (appendToParent == true)
		{
			final String loggerFirstParentName = LoggerUtil.getLoggerFirstParentName(loggerName);
			LoggerUtil.getLogger(loggerFirstParentName, false);
		}

		return logger;
	}

	private static String getLoggerFirstParentName(final String loggerName)
	{
		final int lastDotIndex = loggerName.lastIndexOf(".");
		final String parentName = loggerName.substring(0, lastDotIndex);

		return parentName;
	}

	private static void addAppenders(final Logger logger)
	{
		LoggerUtil.addFileAppenderToLogger(logger);
		LoggerUtil.addLogViewerAppender(logger);
	}

	private static void addFileAppenderToLogger(final Logger logger)
	{
		final String appenderName = LoggerUtil.FILE_APPENDER_PREFIX + logger.getName();
		if (logger.getAppender(appenderName) == null)
		{
			final LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

			final PatternLayoutEncoder encoder = new PatternLayoutEncoder();
			encoder.setContext(context);
			encoder.setPattern("%d|%-4relative|[%thread]|%logger{35}|%-5level|%msg%n");
			encoder.start();

			final String logFilePath = LoggerUtil.getLoggerOutputFilepath(logger);
			final FileAppender<ILoggingEvent> appender = new FileAppender<ILoggingEvent>();
			appender.setName(appenderName);
			appender.setFile(logFilePath);
			appender.setContext(context);
			appender.setEncoder(encoder);

			appender.start();

			logger.addAppender(appender);
		}
	}

	private static String getLoggerOutputFilepath(final Logger logger)
	{
		if (!logger.getName().isEmpty())
		{
			final String scenarioResultFolderPath = LabManagerMaster.getScenarioResultFolderPath();
			final StringBuilder result = new StringBuilder()
							.append(scenarioResultFolderPath)
							.append("\\")
							.append(logger.getName().replace(".", System.getProperty("file.separator")))
							.append(".")
							.append(LoggerUtil.FILE_EXTENSION);

			return result.toString();
		}
		throw new RuntimeException("A logger must have a non-empty name.");
	}

	private static void addLogViewerAppender(final Logger logger)
	{
		if (LoggerUtil.LOG_VIEWER != null)
		{
			final String appenderName = LoggerUtil.GUI_APPENDER_PREFIX + logger.getName();
			if (logger.getAppender(appenderName) == null)
			{
				final LogViewerAppender appender = new LogViewerAppender(LoggerUtil.LOG_VIEWER, logger.getName());
				final LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
				appender.setContext(context);
				appender.setName(appenderName);
				appender.start();

				logger.addAppender(appender);
			}
		}
	}

	private LoggerUtil()
	{
	}

}

/**
 * 
 */
package ca.cyberseclab.avp.util.setting;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author sfrenette
 */
public class Settings
{

	//TODO we need to rename the key. Those that take a directory should be called XYPath, but those requiring a file name should be called something else.

	//TODO We need a way to avoid having to use the '\' escape character in the setting file (Ex: C\:temp\\test.txt).

	public final static String ADB_PATH_KEY = "adbPath";

	public final static String AAPT_PATH_KEY = "aaptPath";

	public final static String CUSTOM_EMULATOR_PATH_KEY = "customEmulatorPath";
	public final static String EMULATOR_PATH_KEY = "emulatorPath";

	public final static String GOOGLE_AVD_PATH_KEY = "googleAVDPath";

	public final static String SCENARIO_RESULTS_PATH_KEY = "scenarioResultsPath";
	public final static String ANDROID_SDK_HOME_PATH_KEY = "androidSDKHome";

	//<lafrancef>
	public final static String GENYMOTION_PATH_KEY = "genymotionPlayerPath";
	public final static String GENYSHELL_PATH_KEY = "genyshellPath";
	public final static String VBOX_MANAGE_PATH_KEY = "vBoxManagePath";

	public final static String TAINTDROID_IMAGES_PATH_KEY = "taintdroidImagesPath";
	//</lafrancef>

	private static Properties settings = new Properties();

	private Settings()
	{

	}

	public static void load(final String filePath) throws IOException
	{
		final File file = new File(filePath);
		Settings.settings.load(new FileInputStream(file));
	}

	public static boolean contains(final String key)
	{
		return Settings.settings.containsKey(key);
	}

	public static String get(final String key)
	{
		if (Settings.contains(key))
		{
			return Settings.settings.getProperty(key);
		}
		else
		{
			throw new SettingsException(String.format("No such setting as \"%s\".", key));
		}
	}
}

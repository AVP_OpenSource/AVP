/**
 * 
 */
package ca.cyberseclab.avp.util.setting;

/**
 * @author sfrenette
 */
public class SettingsException extends RuntimeException
{
	private static final long serialVersionUID = 1198722193387339169L;

	public SettingsException()
	{
	}

	public SettingsException(final String message)
	{
		super(message);
	}

}
